#!/bin/sh

#fix unix 0 timestamp with all resources files
#https://github.com/spotify/docker-maven-plugin/issues/58
#https://spring.io/guides/gs/spring-boot-docker/
touch /data/app.war

JAVA_OPTS="$JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -XX:+PrintGCDateStamps -verbose:gc -XX:+PrintGCDetails -Xloggc:"/var/log/app/gc.log" -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=100M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/var/log/app/catalog.hprof -Dcom.sun.management.jmxremote.port=3556 -Dcom.sun.management.jmxremote.authenticate=false"
COMMAND="java $JAVA_OPTS -jar /data/app.war"
if [ -z "$SPRING_PROFILES_ACTIVE" ]; then
    echo "use default profile"
	$COMMAND >> /var/log/app/app.log 2>&1
else
    echo "use profile:"
    echo $SPRING_PROFILES_ACTIVE
    $COMMAND --spring.profiles.active=$SPRING_PROFILES_ACTIVE >> /var/log/app/app.log 2>&1
fi
