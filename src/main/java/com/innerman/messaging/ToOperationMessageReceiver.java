package com.innerman.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.bus.Event;
import reactor.fn.Consumer;

/**
 * Created by petrpopov on 19/09/16.
 */
public class ToOperationMessageReceiver implements Consumer<Event<ScooterMessage>> {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MessagePublisher messagePublisher;

    @Override
    public void accept(Event<ScooterMessage> mes) {

        ScooterMessage data = mes.getData();
        logger.info("Received TO message {} for scooter {}", data.getData(), data.getCpuid());

        messagePublisher.publishToScooterMessage(data);
    }
}
