package com.innerman.messaging;

import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttClientPersistence;
import org.eclipse.paho.client.mqttv3.MqttException;

/**
 * Created by petrpopov on 06/09/16.
 */
public class MqttClientService extends MqttClient {

    public MqttClientService(String serverURI, String clientId) throws MqttException {
        super(serverURI, clientId);
    }

    public MqttClientService(String serverURI, String clientId, MqttClientPersistence persistence) throws MqttException {
        super(serverURI, clientId, persistence);
    }


    public void subscribe(String[] topicFilters, IMqttMessageListener messageListener) throws MqttException {
        IMqttMessageListener[] lst = new IMqttMessageListener[topicFilters.length];
        for(int i = 0; i < lst.length; i++) {
            lst[i] = messageListener;
        }
        super.subscribe(topicFilters, lst);
    }
}
