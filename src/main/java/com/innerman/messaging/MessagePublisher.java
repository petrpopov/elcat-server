package com.innerman.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.bus.Event;
import reactor.bus.EventBus;

/**
 * Created by petrpopov on 06/09/16.
 */

@Service
public class MessagePublisher {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Value("${reactor.from.scooter.queue}")
    private String fromScooterQueue;

    @Value("${reactor.from.operation.queue}")
    private String fromOperationQueue;

    @Value("${reactor.to.scooter.queue}")
    private String toScooterQueue;

    @Value("${reactor.to.operation.queue}")
    private String toOperationQueue;

    @Autowired
    private EventBus eventBus;

    public void publishFromScooterMessage(ScooterMessage mes) {
        logger.info("Publishing FROM scooter message {}", mes);
        eventBus.notify(fromScooterQueue, Event.wrap(mes));
    }

    public void publishFromOperationMessage(ScooterMessage mes) {
        logger.info("Publishing FROM operation message {}", mes);
        eventBus.notify(fromOperationQueue, Event.wrap(mes));
    }

    public void publishToScooterMessage(ScooterMessage mes) {
        logger.info("Publishing TO scooter message {}", mes);
        eventBus.notify(toScooterQueue, Event.wrap(mes));
    }

    public void publishToOperationMessage(ScooterMessage mes) {
        logger.info("Publishing TO operation message {}", mes);
        eventBus.notify(toOperationQueue, Event.wrap(mes));
    }
}
