package com.innerman.messaging;

import com.innerman.operations.Operations;
import org.eclipse.paho.client.mqttv3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


@Service
public class MqttService {

    private static final int QOS = 1; // http://www.hivemq.com/blog/mqtt-essentials-part-6-mqtt-quality-of-service-levels

    private Logger logger = LoggerFactory.getLogger(getClass());
    private volatile MqttClientService client = null;
    private ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
    private volatile boolean needToRepublish = true; // publish again if publishing failed

    @Value("${mqtt.server}")
    private String server;

    @Value("${mqtt.clientid}")
    private String clientId;

    @Value("${mqtt.topicfilters}")
    private String lastWillTopic;  // http://www.hivemq.com/blog/mqtt-essentials-part-9-last-will-and-testament
    private String[] topicFilters = null; // array of topics to subscribe

    @Autowired
    private Operations operations;

    @Autowired
    private MessagePublisher messagePublisher;


    @PostConstruct
    public void initMqtt() throws MqttException {

        topicFilters = new String[] {lastWillTopic};

        try {
            client = new MqttClientService(server, (clientId != null) ? clientId : MqttClient.generateClientId(), null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        client.setCallback(new MqttCallback() {
            public void connectionLost(Throwable throwable) {
                logger.info("mqtt connection lost");
                connect();
            }

            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                String sMessage = new String(mqttMessage.getPayload());
                logger.debug(topic + ": " + sMessage);
            }

            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                logger.debug("mqtt delivery completed");
            }
        });

        connect();
    }

    public void publish(final String topic, final String message, final boolean retained) {
        publishMessage(topic, message, retained, false);
    }

    public void connect() {
        executor.submit((Runnable) () -> {
            while (!doConnectToServer()) {
                //delay between reconnects
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            try {
                List<String> readTopics = operations.getMainReadTopics();
                client.subscribe(readTopics.toArray(new String[readTopics.size()]), (topic, message) -> {
                    logger.info("MQTT message {} in topic {}", message.toString(), topic);
                    messagePublisher.publishFromScooterMessage(
                            ScooterMessage.fromScooterMessage(topic, message.toString())
                    );
                });
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void disconnect() {
        if (client != null) {
            executor.submit((Runnable) () -> {
                try {
                    if (lastWillTopic != null) {
                        client.publish(lastWillTopic, "false".getBytes(), QOS, true);
                    }

                    client.disconnect();
                    logger.info("mqtt disconnected!");

                    executor.shutdownNow();
                    client = null;
                } catch (MqttException e) {
                    logger.error(e.getLocalizedMessage());
                }
            });
        }
    }

    private boolean doConnectToServer() {

        try {
            MqttConnectOptions conOpt = new MqttConnectOptions();
            conOpt.setCleanSession(true);
            conOpt.setKeepAliveInterval(10);
            conOpt.setConnectionTimeout(5);

            client.connect(conOpt);
            logger.info("mqtt connected!");

            if (topicFilters != null) {
                client.subscribe(topicFilters);
            }
        }
        catch (MqttException e) {
            logger.error(e.getMessage());
            return false;
        }

        return true;
    }

    private void publishMessage(final String topic, final String message, final boolean retained, final boolean dup) {

        executor.schedule((Runnable) () -> {
            logger.info("Publishing TO message for topic {} and message {}", topic, message);
            try {
                if (!client.isConnected()) {
                    throw new MqttException(MqttException.REASON_CODE_CLIENT_NOT_CONNECTED);
                }

                MqttMessage mqttMessage = new MqttMessage(message.getBytes());
                mqttMessage.setRetained(retained);

                client.publish(topic, mqttMessage);
            } catch (Exception e) {
                logger.error(e.getMessage());
                if (needToRepublish) {
                    logger.info("republishing");
                    publishMessage(topic, message, retained, true);
                }
            }
        }, dup ? 1 : 0, TimeUnit.SECONDS);
    }
}