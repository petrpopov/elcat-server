package com.innerman.messaging;

import com.innerman.operations.Command;
import com.innerman.service.ScooterCommandService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.bus.Event;
import reactor.fn.Consumer;

/**
 * Created by petrpopov on 19/09/16.
 */
public class ToScooterMessageReceiver implements Consumer<Event<ScooterMessage>> {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MqttService mqttService;

    @Autowired
    private ScooterCommandService scooterCommandService;


    @Override
    public void accept(Event<ScooterMessage> mes) {

        ScooterMessage data = mes.getData();
        logger.info("Received TO message {} for scooter {}", data.getData(), data.getCpuid());

        Command command = data.getCommand();
        String topic = data.getCpuid() + "/" + command.getCommandName();

        Object value = scooterCommandService.getCommandValue(command);
        if(value == null) {
            logger.error("Incorrect TO command {} for scooter {}", command, data.getCpuid());
            return;
        }

        mqttService.publish(topic, command.getCommandValue(), true);
    }
}
