package com.innerman.messaging;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.innerman.operations.Command;
import com.innerman.operations.CommandDirection;
import com.innerman.operations.CommandName;
import com.innerman.utils.DateUtils;
import com.innerman.utils.ElcatException;
import org.eclipse.paho.client.mqttv3.util.Strings;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by petrpopov on 06/09/16.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ScooterMessage implements Serializable {

    private String cpuid;
    private String topic;
    private Date received;
    private String data;
    private Command command;

    public ScooterMessage() {
    }

    public static ScooterMessage fromScooterMessage(String topic, String data) throws ElcatException {

        return doCreateScooterMessage(CommandDirection.FROM_SCOOTER, topic, data);
    }

    public static ScooterMessage toScooterMessage(String cpuid, String topic, String data) throws ElcatException {
        ScooterMessage scooterMessage = doCreateScooterMessage(CommandDirection.TO_SCOOTER, topic, data);
        scooterMessage.setCpuid(cpuid);
        return scooterMessage;
    }

    private static ScooterMessage doCreateScooterMessage(CommandDirection direction, String topic, String data) throws ElcatException {

        ScooterMessage res = new ScooterMessage();
        res.topic = topic;
        res.data = data;

        try {
            res.command = new Command(direction, getTopicFromTopic(topic), data);
        }
        catch (Exception e) {
            throw new ElcatException("unknown command");
        }

        res.cpuid = getCpuIdFromTopic(topic);
        res.received = DateUtils.getNowUTC();
        return res;
    }

    public static String getCpuIdFromTopic(String topic) {

        if(Strings.isEmpty(topic)) {
            return null;
        }

        String[] split = topic.split("/");
        if(split.length < 2) {
            return null;
        }

        return split[0].trim().toLowerCase();
    }

    public static CommandName getTopicFromTopic(String topic) {

        if(Strings.isEmpty(topic)) {
            return null;
        }

        List<String> commands = Arrays.asList(CommandName.values()).stream().map(Enum::toString).collect(Collectors.toList());
        if(commands.contains(topic.trim())) {
            return CommandName.valueOf(commands.get(commands.indexOf(topic.trim())));
        }

        String[] split = topic.split("/");
        if(split.length < 2) {
            return null;
        }

        return CommandName.valueOf(split[1].trim());
    }

    public String getCpuid() {
        return cpuid;
    }

    public void setCpuid(String cpuid) {
        this.cpuid = cpuid;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Date getReceived() {
        return received;
    }

    public void setReceived(Date received) {
        this.received = received;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }

    @Override
    public String toString() {
        return "ScooterMessage{" +
                "cpuid='" + cpuid + '\'' +
                ", topic='" + topic + '\'' +
                ", data='" + data + '\'' +
                '}';
    }
}
