package com.innerman.messaging;

import com.innerman.service.ScooterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.bus.Event;
import reactor.fn.Consumer;

/**
 * Created by petrpopov on 06/09/16.
 */
public class FromScooterMessageReceiver implements Consumer<Event<ScooterMessage>> {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MessagePublisher publisher;

    @Autowired
    private ScooterService scooterService;

    @Override
    public void accept(Event<ScooterMessage> mes) {

        ScooterMessage data = mes.getData();
        logger.info("Received FROM message {} for scooter {}", data.getData(), data.getCpuid());

        scooterService.getOrCreate(data.getCpuid());
        publisher.publishFromOperationMessage(mes.getData());
    }
}
