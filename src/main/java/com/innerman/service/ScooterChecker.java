package com.innerman.service;

import com.innerman.entity.OperationEntity;
import com.innerman.entity.ScooterEntity;
import com.innerman.operations.CommandDirection;
import com.innerman.repository.OperationRepository;
import com.innerman.repository.ScooterRepository;
import com.innerman.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by petrpopov on 12/09/16.
 */

@Service
public class ScooterChecker {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ScooterRepository scooterRepository;

    @Autowired
    private OperationRepository operationRepository;

    //todo make in memory
    //once in a minute
    @Scheduled(fixedDelay = 60*1000)
    @Transactional
    public void checkScooters() {

        logger.info("Start checking online status for scooters");

        List<ScooterEntity> scooters = (List<ScooterEntity>) scooterRepository.findAll();
        logger.info("Found {} scooters", scooters.size());

        scooters.forEach(scooter -> {
            OperationEntity op = operationRepository.findTopByScooterAndDirectionOrderByReceivedDesc(scooter, CommandDirection.FROM_SCOOTER);

            if(op == null) {
                scooter.setConnected(false);
                scooterRepository.save(scooter);
            }
            else {
                Date received = op.getReceived();
                long diff = DateUtils.getDateDifferenceInSeconds(received, DateUtils.getNowUTC());
                if(diff >= 60) {
                    scooter.setConnected(false);
                }
                else {
                    scooter.setConnected(true);
                }
            }
            scooterRepository.save(scooters);
        });

        logger.info("End of checking online status for scooters");
    }
}
