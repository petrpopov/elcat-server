package com.innerman.service;

import com.innerman.dto.DashboardDTO;
import com.innerman.dto.ScooterDTO;
import com.innerman.dto.ScooterInfoDTO;
import com.innerman.dto.TripDTO;
import com.innerman.entity.OperationEntity;
import com.innerman.entity.ScooterEntity;
import com.innerman.entity.TripEntity;
import com.innerman.mappers.ScooterInfoMapper;
import com.innerman.mappers.TripMapper;
import com.innerman.messaging.MessagePublisher;
import com.innerman.messaging.ScooterMessage;
import com.innerman.operations.Command;
import com.innerman.operations.CommandDirection;
import com.innerman.operations.CommandName;
import com.innerman.operations.GpsOperation;
import com.innerman.repository.OperationRepository;
import com.innerman.repository.ScooterRepository;
import com.innerman.repository.TripRepository;
import com.innerman.repository.UserRepository;
import com.innerman.utils.ElcatException;
import com.innerman.utils.GpsUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by petrpopov on 06/09/16.
 */

@Service
public class ScooterService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ScooterRepository scooterRepository;

    @Autowired
    private OperationRepository operationRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TripRepository tripRepository;

    @Autowired
    private ScooterInfoMapper scooterInfoMapper;

    @Autowired
    private TripMapper tripMapper;

    @Autowired
    private MessagePublisher messagePublisher;

    @Autowired
    private ScooterCommandService scooterCommandService;


    @Transactional(propagation = Propagation.REQUIRED)
    public DashboardDTO getScootersTotalInfo() {

        DashboardDTO res = new DashboardDTO();
        res.setScooters(scooterRepository.count());
        res.setUsers(userRepository.count());
        res.setDriveMinutes(tripRepository.getTripsTotalDuration());
        return res;
    }


    @Transactional(propagation = Propagation.REQUIRED)
    public List<ScooterEntity> getScooters() {
        return (List<ScooterEntity>) scooterRepository.findAll();
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public ScooterInfoDTO getScooterInfo(String cpuid) throws ElcatException {

        ScooterEntity scooter = findByCpuid(cpuid);
        if(scooter == null) {
            throw new ElcatException("Scooter id {} not found!", cpuid);
        }
        scooter = scooterRepository.findOne(scooter.getId());

        try {
            return scooterInfoMapper.toScooterInfoDTO(scooter);
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<TripDTO> getTrips(String cpuid) {

        List<TripDTO> res = new ArrayList<>();
        ScooterEntity scooter = findByCpuid(cpuid);
        if(scooter == null) {
            return res;
        }

        List<TripEntity> trips = tripRepository.findByScooterOrderByCreatedAtDesc(scooter);
        int i = 1;
        trips.forEach(t -> {
            res.add(tripMapper.toTripDto(t));
            res.get(i-1).setNumber(i);
        });
        return res;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @CacheEvict(value = "scooters")
    public ScooterEntity getOrCreate(String cpuid) {

        logger.info("Searching for scooter {} and creating if needed", cpuid);

        ScooterEntity scooter = scooterRepository.findByCpuid(cpuid);
        if(scooter == null) {
            logger.info("Scooter {} not found, creating", cpuid);
            scooter = doCreateScooter(cpuid);
            scooter.setConnected(true);
            scooterRepository.save(scooter);
        }

        return scooter;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @CacheEvict(value = "scooters")
    public ScooterEntity updateScooter(String id, ScooterDTO dto) throws ElcatException {

        logger.info("Updating scooter {}", id);

        ScooterEntity scooter = scooterRepository.findByCpuid(id);
        if(scooter == null) {
            throw new ElcatException("Scooter " + id + " not found!");
        }
        scooter = scooterRepository.findOne(scooter.getId());

        scooterCommandService.updateFields(scooter, dto);
        scooterRepository.save(scooter);
        return scooter;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void runToCommand(String id, Command command) throws ElcatException {

        ScooterEntity scooter = scooterRepository.findByCpuid(id);
        if(scooter == null) {
            throw new ElcatException("Scooter " + id + " not found!");
        }

        ScooterMessage mes = ScooterMessage.toScooterMessage(scooter.getCpuid(), command.getCommandName().toString(), command.getCommandValue());
        saveOperationAndScooter(mes);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void saveOperationAndScooter(ScooterMessage mes) {

        ScooterEntity scooter = scooterRepository.findByCpuid(mes.getCpuid());
        if(scooter == null) {
            return;
        }
        scooter = scooterRepository.findOne(scooter.getId());

        if(mes.getCommand().getDirection().equals(CommandDirection.TO_SCOOTER)) {
            String name = mes.getCommand().getCommandName().toString();
            Boolean correct = scooterCommandService.correctWriteCommand(name);
            if(!correct) {
                logger.error("Incorrect TO command {}", name);
                return;
            }
        }

        saveOperation(scooter, mes);
        saveScooter(scooter, mes);

        if(mes.getCommand().getDirection().equals(CommandDirection.TO_SCOOTER)) {
            messagePublisher.publishToOperationMessage(mes);
        }
    }

    private OperationEntity saveOperation(ScooterEntity scooter, ScooterMessage mes) {

        logger.info("Saving operation " + mes.getTopic() + " for scooter " + scooter.getCpuid());

        OperationEntity op = new OperationEntity();
        op.setScooter(scooter);
        op.setReceived(mes.getReceived());
        op.setCommand(mes.getCommand().toJsonMap());
        op.setDirection(mes.getCommand().getDirection());

        if(mes.getCommand().getCommandName().equals(CommandName.gps)) {
            GpsOperation gp = GpsUtil.fromStringToGpsOperation(mes.getCommand().getCommandValue());
            op.setScooterTimestamp(new Date(gp.getTime()*1000));
            op.setLocation(GpsUtil.fromGpsOperationToPoint(gp));
        }

        operationRepository.save(op);
        return op;
    }

    @CacheEvict(value = "scooters")
    private ScooterEntity saveScooter(ScooterEntity scooter, ScooterMessage mes) {

        CommandName commandName = mes.getCommand().getCommandName();
        logger.info("Updating scooter {} for command {}", scooter.getCpuid(), commandName);

        scooterCommandService.changeScooterForCommand(scooter, mes.getCommand());

        scooterRepository.save(scooter);
        return scooter;
    }

    private ScooterEntity doCreateScooter(String cpuid) {

        ScooterEntity scooter = new ScooterEntity();
        scooter.setCpuid(cpuid);
        scooter.setActive(false);
        scooter.setDebug(false);
        scooter.setBatteryLevel(-1L);
        scooter.setLastSpeed(-1L);
        scooter.setStolen(false);
        scooter.setHornTimer(-1L);
        scooter.setWheelRadius(600L);
        scooter.setMinVoltage(-1L);
        scooter.setMaxVoltage(-1L);
        scooter.setPowerEnable(false);
        scooter.setHeadLight(-1L);
        scooter.setHornTimer(-1L);
        return scooter;
    }

    private ScooterEntity findByCpuid(String cpuid) {
        ScooterEntity scooter = scooterRepository.findByCpuid(cpuid);
        if (scooter == null) {
            return scooterRepository.findOne(Long.parseLong(cpuid));
        }
        return scooter;
    }
}
