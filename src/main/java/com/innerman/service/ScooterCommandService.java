package com.innerman.service;

import com.innerman.dto.ScooterDTO;
import com.innerman.entity.ScooterEntity;
import com.innerman.operations.*;
import com.innerman.utils.GpsUtil;
import org.eclipse.paho.client.mqttv3.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by petrpopov on 18/09/16.
 */

@Service
public class ScooterCommandService {

    private Logger logger = LoggerFactory.getLogger(getClass());
    private Map<CommandName, CommandField> commands = new HashMap<>();

    @Autowired
    private Operations operations;

    @PostConstruct
    public void init() {

        commands.put(CommandName.title, new CommandField("title", CommandType.BOOLEAN, Boolean.TRUE));
        commands.put(CommandName.connected, new CommandField("connected", CommandType.BOOLEAN));
        commands.put(CommandName.debug, new CommandField("debug", CommandType.BOOLEAN));
        commands.put(CommandName.stolen, new CommandField("stolen", CommandType.BOOLEAN, Boolean.TRUE));
        commands.put(CommandName.wheelCircumference, new CommandField("wheelRadius", CommandType.LONG, Boolean.TRUE));
        commands.put(CommandName.minVoltage, new CommandField("minVoltage", CommandType.LONG, Boolean.TRUE));
        commands.put(CommandName.maxVoltage, new CommandField("maxVoltage", CommandType.LONG, Boolean.TRUE));
        commands.put(CommandName.Odometer, new CommandField("odometer", CommandType.LONG));
        commands.put(CommandName.BatteryLevel, new CommandField("batteryLevel", CommandType.LONG));
        commands.put(CommandName.PowerEnable, new CommandField("powerEnable", CommandType.BOOLEAN, Boolean.TRUE));
        commands.put(CommandName.HornTimer, new CommandField("hornTimer", CommandType.LONG, Boolean.TRUE));
        commands.put(CommandName.Headlight, new CommandField("headLight", CommandType.LONG, Boolean.TRUE));
        commands.put(CommandName.gps, new CommandField("geoLocation", CommandType.GEOLOCATION));
        commands.put(CommandName.Speed, new CommandField("lastSpeed", CommandType.LONG));
        commands.put(CommandName.OdometerTrip, new CommandField("odometerTrip", CommandType.LONG));
    }

    public ScooterEntity updateFields(ScooterEntity scooter, ScooterDTO dto) {

        Field[] fields = dto.getClass().getDeclaredFields();
        for (Field field : fields) {
            Optional<Map.Entry<CommandName, CommandField>> opt = commands
                    .entrySet()
                    .stream()
                    .filter(c -> c.getValue().getField().equals(field.getName())
                        && c.getValue().getUpdateable() == Boolean.TRUE)
                    .findFirst();
            if(!opt.isPresent()) {
                continue;
            }

            Map.Entry<CommandName, CommandField> entry = opt.get();

            try {
                field.setAccessible(true);
                Object val = field.get(dto);

                Field targetField = scooter.getClass().getDeclaredField(entry.getValue().getField());
                targetField.setAccessible(true);
                targetField.set(scooter, val);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return scooter;
    }

    public Boolean correctWriteCommand(String commandName) {
        return operations.getMainWriteTopics()
                .stream().map(s -> s.replace("+/", ""))
                .collect(Collectors.toList())
                .contains(commandName);
    }

    public ScooterEntity changeScooterForCommand(ScooterEntity scooter, Command command) {

        logger.info("Chaging scooter {} for command {}", scooter.getCpuid(), command);
        CommandName commandName = command.getCommandName();

        CommandField field = commands.get(commandName);
        Object value = getCommandValue(command);
        if(value == null) {
            logger.error("Could not process command value {} for field {}", command.getCommandValue(), field.getField());
            return scooter;
        }

        if(value instanceof GpsOperation) {
            scooter.setLastLocation(GpsUtil.fromGpsOperationToPoint((GpsOperation) value));
            return scooter;
        }

        setField(scooter, field.getField(), value);
        return scooter;
    }

    public Object getCommandValue(Command command) {

        CommandName commandName = command.getCommandName();

        CommandField field = commands.get(commandName);
        if(field == null) {
            logger.error("Not found any declared command {}", command);
            return null;
        }

        return getValue(command, field);
    }

    private Object getValue(Command command, CommandField field) {

        String val = command.getCommandValue();
        if(Strings.isEmpty(val)) {
            return null;
        }

        Object res = null;

        try {
            switch (field.getType()) {
                case BOOLEAN:
                    res = Boolean.parseBoolean(val.trim().toLowerCase());
                    break;
                case LONG:
                    res = Long.parseLong(val.trim());
                    break;
                case STRING:
                    res = val.trim();
                    break;
                case GEOLOCATION:
                    res = GpsUtil.fromStringToGpsOperation(val.trim());
                    break;
            }
        }
        catch (Exception e) {
            return null;
        }

        return res;
    }

    private void setField(ScooterEntity scooter, String field, Object value) {

        try {
            Field declaredField = scooter.getClass().getDeclaredField(field);
            declaredField.setAccessible(true);
            declaredField.set(scooter, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
