package com.innerman.operations;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * Created by petrpopov on 23/09/2016.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class GpsOperation implements Serializable {

    private Long time;
    private Double lat;
    private Double lon;

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }
}
