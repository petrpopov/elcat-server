package com.innerman.operations;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.innerman.utils.jdbc.JsonMap;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by petrpopov on 07/09/16.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Command implements Serializable {

    private CommandDirection direction;
    private CommandName commandName;
    private String commandValue;

    public Command() {
    }

    public Command(CommandDirection direction, CommandName commandName, String commandValue) {
        this.direction = direction;
        this.commandName = commandName;
        this.commandValue = commandValue;
    }

    public JsonMap toJsonMap() {

        HashMap<String, String> map = new HashMap<>();
        map.put(commandName.toString(), commandValue);
        return new JsonMap(map);
    }

    public CommandDirection getDirection() {
        return direction;
    }

    public void setDirection(CommandDirection direction) {
        this.direction = direction;
    }

    public CommandName getCommandName() {
        return commandName;
    }

    public void setCommandName(CommandName commandName) {
        this.commandName = commandName;
    }

    public String getCommandValue() {
        return commandValue;
    }

    public void setCommandValue(String commandValue) {
        this.commandValue = commandValue;
    }

    @Override
    public String toString() {
        return "Command{" +
                "commandName=" + commandName +
                ", commandValue='" + commandValue + '\'' +
                '}';
    }
}
