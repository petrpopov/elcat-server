package com.innerman.operations;

/**
 * Created by petrpopov on 06/09/16.
 */
public enum OperationType {
    R,
    W,
    RW
}
