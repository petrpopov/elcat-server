package com.innerman.operations;

/**
 * Created by petrpopov on 07/09/16.
 */
public enum CommandName {
    connected,
    debug,
    stolen,
    wheelCircumference,
    minVoltage,
    maxVoltage,
    Odometer,
    BatteryLevel,
    gps,
    PowerEnable,
    HornTimer,
    title, Headlight,
    Speed,
    OdometerTrip
}
