package com.innerman.operations;

import com.google.common.base.Splitter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by petrpopov on 06/09/16.
 */

@Component
public class Operations {

    @Value("${mqtt.topics}")
    private String topicStrings;
    private Map<String, List<OperationType>> mainTopics;

    @PostConstruct
    public void init() {

        Map<String, List<String>> listMap = parseMap(topicStrings);
        mainTopics = new HashMap<>();
        listMap.entrySet().forEach(topic -> {
            List<OperationType> vals = new ArrayList<>();
            topic.getValue().forEach(val -> vals.add(OperationType.valueOf(val)));
            mainTopics.put(topic.getKey(), vals);
        });
    }

    public Map<String, List<OperationType>> getMainTopics() {
        return mainTopics;
    }

    public List<String> getMainReadTopics() {
        return mainTopics.entrySet()
                .stream()
                .filter(p -> p.getValue().contains(OperationType.R))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public List<String> getMainWriteTopics() {
        return mainTopics.entrySet()
                .stream()
                .filter(p -> p.getValue().contains(OperationType.W))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    private Map<String, List<String>> parseMap(String formattedMap) {
        Map<String, String> split = Splitter.on(",").withKeyValueSeparator("=").split(formattedMap);
        Map<String, List<String>> res = new HashMap<>();
        split.entrySet().forEach(e -> res.put(e.getKey(), Arrays.asList(e.getValue().split(":"))));
        return res;
    }
}
