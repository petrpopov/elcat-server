package com.innerman.operations;

/**
 * Created by petrpopov on 19/09/16.
 */
public enum CommandDirection {
    FROM_SCOOTER,
    TO_SCOOTER
}
