package com.innerman.operations;

/**
 * Created by petrpopov on 19/09/16.
 */
public enum CommandType {

    BOOLEAN,
    LONG,
    STRING,
    GEOLOCATION
}
