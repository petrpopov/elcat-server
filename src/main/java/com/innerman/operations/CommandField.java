package com.innerman.operations;

import java.io.Serializable;

/**
 * Created by petrpopov on 19/09/16.
 */
public class CommandField implements Serializable {

    private String field;
    private CommandType type;
    private Boolean updateable = Boolean.FALSE;

    public CommandField() {
    }

    public CommandField(String field, CommandType type) {
        this.field = field;
        this.type = type;
        this.updateable = false;
    }

    public CommandField(String field, CommandType type, Boolean updateable) {
        this.field = field;
        this.type = type;
        this.updateable = updateable;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public CommandType getType() {
        return type;
    }

    public void setType(CommandType type) {
        this.type = type;
    }

    public Boolean getUpdateable() {
        return updateable;
    }

    public void setUpdateable(Boolean updateable) {
        this.updateable = updateable;
    }
}
