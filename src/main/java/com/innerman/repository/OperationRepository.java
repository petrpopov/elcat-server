package com.innerman.repository;

import com.innerman.entity.OperationEntity;
import com.innerman.entity.ScooterEntity;
import com.innerman.operations.CommandDirection;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by petrpopov on 05/09/16.
 */

@Repository
public interface OperationRepository extends PagingAndSortingRepository<OperationEntity, Long> {

    OperationEntity findTopByScooterAndDirectionOrderByReceivedDesc(ScooterEntity scooter, CommandDirection direction);
}
