package com.innerman.repository;

import com.innerman.entity.ScooterEntity;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by petrpopov on 05/09/16.
 */

@Repository
public interface ScooterRepository extends PagingAndSortingRepository<ScooterEntity, Long> {

    @Cacheable("scooters")
    ScooterEntity findByCpuid(String cpuid);
}
