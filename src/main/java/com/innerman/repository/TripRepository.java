package com.innerman.repository;

import com.innerman.entity.ScooterEntity;
import com.innerman.entity.TripEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by petrpopov on 05/09/16.
 */

@Repository
public interface TripRepository extends PagingAndSortingRepository<TripEntity, Long> {

    List<TripEntity> findByScooterOrderByCreatedAtDesc(ScooterEntity scooter);

    @Query(value = "select sum(duration) from trips where completed is true", nativeQuery = true)
    Long getTripsTotalDuration();
}
