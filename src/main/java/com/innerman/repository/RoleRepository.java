package com.innerman.repository;


import com.innerman.entity.RoleEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by petrpopov on 03.09.15.
 */

@Repository
public interface RoleRepository extends PagingAndSortingRepository<RoleEntity, Long> {

    RoleEntity findByTitle(String title);
}
