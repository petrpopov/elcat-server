package com.innerman.controllers;

import com.innerman.dto.ScooterDTO;
import com.innerman.dto.ScooterInfoDTO;
import com.innerman.dto.TripDTO;
import com.innerman.entity.ScooterEntity;
import com.innerman.mappers.ScooterMapper;
import com.innerman.mappers.TripMapper;
import com.innerman.operations.Command;
import com.innerman.service.ScooterService;
import com.innerman.utils.ElcatException;
import org.geolatte.geom.M;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by petrpopov on 08/09/16.
 */

@Component
@RestController
public class ApiController {

    @Autowired
    private ScooterService scooterService;

    @Autowired
    private ScooterMapper scooterMapper;


    @GetMapping(value = "/api/dashboard", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object dashboard() {
        return Message.success(scooterService.getScootersTotalInfo());
    }


    @GetMapping(value = "/api/scooters", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object scooters() {

        List<ScooterEntity> scooters = scooterService.getScooters();
        List<ScooterDTO> res = scooters
                .stream()
                .map(s -> scooterMapper.toScooterDTO(s))
                .collect(Collectors.toList());
        return Message.success(res);
    }

    @GetMapping(value = "/api/scooters/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object scooterInfo(@NotNull @PathVariable("id") String id) throws ElcatException {

        ScooterInfoDTO scooter = scooterService.getScooterInfo(id);
        return Message.success(scooter);
    }

    @GetMapping(value = "/api/scooters/{id}/trips", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object trips(@NotNull @PathVariable("id") String id) {

        List<TripDTO> res = scooterService.getTrips(id);
        return Message.success(res);
    }

    @PostMapping(value = "/api/scooters/{id}/command", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object sendCommand(@NotNull @PathVariable("id") String id,
                              @Valid @RequestBody Command command) throws ElcatException {

        scooterService.runToCommand(id, command);
        return Message.success();
    }

    @PostMapping(value = "/api/scooters/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object saveScooter(@NotNull @PathVariable("id") String id,
                              @Valid @RequestBody ScooterDTO scooter) throws ElcatException {

        ScooterEntity scooterEntity = scooterService.updateScooter(id, scooter);
        return Message.success(scooterMapper.toScooterDTO(scooterEntity));
    }
}
