package com.innerman.controllers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * Created by petrpopov on 08/09/16.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Message implements Serializable {

    private Integer status;
    private Object result;

    public Message() {
    }

    public Message(Integer status) {
        this.status = status;
    }

    public Message(Integer status, Object result) {
        this.status = status;
        this.result = result;
    }

    public static Message success() {
        return new Message(200);
    }

    public static Message success(Object result) {
        return new Message(200, result);
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
