package com.innerman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by petrpopov on 25/08/16.
 */

@SpringBootApplication
@EnableJpaRepositories
@EnableCaching
@EnableScheduling
public class ElcatApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(ElcatApplication.class, args);
    }
}
