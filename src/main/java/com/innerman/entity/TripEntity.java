package com.innerman.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by petrpopov on 05/09/16.
 */

@Entity(name = "trips")
@SequenceGenerator(sequenceName = "trips_sequence", name = "default_generator", allocationSize = 1)
@Access(AccessType.FIELD)
public class TripEntity extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY, optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "scooter_id")
    private ScooterEntity scooter;

    @Column(name = "start_date", nullable = false, updatable = false, columnDefinition = "timestamp")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date start;

    @Column(name = "end_date", nullable = false, updatable = false, columnDefinition = "timestamp")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date finish;

    @Column(name = "duration")
    private Long duration;

    @Column(name = "completed", nullable = false)
    private Boolean completed;

    public ScooterEntity getScooter() {
        return scooter;
    }

    public void setScooter(ScooterEntity scooter) {
        this.scooter = scooter;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getFinish() {
        return finish;
    }

    public void setFinish(Date finish) {
        this.finish = finish;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }
}
