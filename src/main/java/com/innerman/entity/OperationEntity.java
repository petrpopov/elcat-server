package com.innerman.entity;

import com.innerman.operations.CommandDirection;
import com.innerman.utils.jdbc.JsonMap;
import com.innerman.utils.jdbc.JsonMapType;
import com.vividsolutions.jts.geom.Point;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by petrpopov on 05/09/16.
 */

@Entity(name = "operations")
@SequenceGenerator(sequenceName = "operations_sequence", name = "default_generator", allocationSize = 1)
@Access(AccessType.FIELD)
@TypeDefs(value = { @TypeDef(name = "JsonMapType", typeClass = JsonMapType.class) })
public class OperationEntity extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY, optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "scooter_id")
    private ScooterEntity scooter;

    @Column(name = "received", nullable = false, updatable = false, columnDefinition = "timestamp")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date received;

    @Column(name = "scooter_timestamp", nullable = true, updatable = false, columnDefinition = "timestamp")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date scooterTimestamp;

    @Column(name = "command")
    @Type(type = "JsonMapType")
    private JsonMap command;

    @Column(name = "direction")
    @Enumerated(value = EnumType.STRING)
    private CommandDirection direction;

    @Column(name = "location", columnDefinition = "Geometry")
    private Point location;


    public ScooterEntity getScooter() {
        return scooter;
    }

    public void setScooter(ScooterEntity scooter) {
        this.scooter = scooter;
    }

    public Date getReceived() {
        return received;
    }

    public void setReceived(Date received) {
        this.received = received;
    }

    public Date getScooterTimestamp() {
        return scooterTimestamp;
    }

    public void setScooterTimestamp(Date scooterTimestamp) {
        this.scooterTimestamp = scooterTimestamp;
    }

    public JsonMap getCommand() {
        return command;
    }

    public void setCommand(JsonMap command) {
        this.command = command;
    }

    public CommandDirection getDirection() {
        return direction;
    }

    public void setDirection(CommandDirection direction) {
        this.direction = direction;
    }

    public Point getLocation() {
        return location;
    }

    public void setLocation(Point location) {
        this.location = location;
    }
}
