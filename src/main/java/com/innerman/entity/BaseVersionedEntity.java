package com.innerman.entity;


import com.innerman.utils.DateUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by petrpopov on 15/12/14.
 */

@MappedSuperclass
public class BaseVersionedEntity implements Serializable {

    protected Date createdAt;
    protected Date updatedAt;

    public BaseVersionedEntity() {
    }

    @Column(name = "created_at", nullable = false, updatable = false, columnDefinition = "timestamp")
    @Temporal(value = TemporalType.TIMESTAMP)
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Column(name = "updated_at", columnDefinition = "timestamp")
    @Temporal(value = TemporalType.TIMESTAMP)
    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @PrePersist
    public void onPrePersist() {
        this.setCreatedAt(DateUtils.getNowUTC());
        this.setUpdatedAt(DateUtils.getNowUTC());
    }

    @PreUpdate
    public void onPreUpdate() {
        this.setUpdatedAt(DateUtils.getNowUTC());
    }
}
