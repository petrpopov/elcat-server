package com.innerman.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by petrpopov on 03.09.15.
 */

@Entity(name = "users")
@SequenceGenerator(sequenceName = "users_sequence", name = "default_generator", allocationSize = 1)
@Access(AccessType.FIELD)
@NamedEntityGraph(name = "user", attributeNodes = {
        @NamedAttributeNode(value = "roles")
})
public class UserEntity extends BaseEntity {

    @Column(name = "email", nullable = false, length = 250)
    private String email;

    @Column(name = "password_hash", nullable = false, length = 250)
    private String hashPassword;

    @Column(name = "salt", nullable = false, length = 250)
    private String salt;

    @Column(name = "active", nullable = false)
    private Boolean active;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "users_roles",
            joinColumns = {@JoinColumn(name = "user_id", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "role_id", nullable = false, updatable = false)})
    @Fetch(FetchMode.JOIN)
    private Set<RoleEntity> roles = new HashSet<>();

    public UserEntity() {
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHashPassword() {
        return hashPassword;
    }

    public void setHashPassword(String hashPassword) {
        this.hashPassword = hashPassword;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Set<RoleEntity> getRoles() {
        return roles;
    }

    public void setRoles(Set<RoleEntity> roles) {
        this.roles = roles;
    }
}
