package com.innerman.entity;

import javax.persistence.*;

/**
 * Created by petrpopov on 03.09.15.
 */

@Entity(name = "roles")
@SequenceGenerator(sequenceName = "roles_sequence", name = "default_generator", allocationSize = 1)
@Access(AccessType.FIELD)
public class RoleEntity extends BaseEntity {

    @Column(name = "title", nullable = false, length = 250)
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
