package com.innerman.entity;

import com.vividsolutions.jts.geom.Point;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by petrpopov on 05/09/16.
 */

@Entity(name = "scooters")
@SequenceGenerator(sequenceName = "scooters_sequence", name = "default_generator", allocationSize = 1)
@Access(AccessType.FIELD)
public class ScooterEntity extends BaseEntity {

    @Column(name = "cpuid", nullable = false, length = 250)
    private String cpuid;

    @Column(name = "title", nullable = true, length = 250)
    private String title;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "debug")
    private Boolean debug;

    @Column(name = "connected")
    private Boolean connected;

    @Column(name = "stolen")
    private Boolean stolen;

    @Column(name = "wheel_radius")
    private Long wheelRadius;

    @Column(name = "min_voltage")
    private Long minVoltage;

    @Column(name = "max_voltage")
    private Long maxVoltage;

    @Column(name = "odometer")
    private Long odometer;

    @Column(name = "battery_level")
    private Long batteryLevel;

    //gps

    @Column(name = "power_enable")
    private Boolean powerEnable;

    @Column(name = "horn_timer")
    private Long hornTimer;

    @Column(name = "head_light")
    private Long headLight;

    //debug commands
    @Column(name = "last_speed", nullable = false)
    private Long lastSpeed;

    @Column(name = "last_location", columnDefinition = "Geometry")
    private Point lastLocation;

    @Column(name = "odometer_trip", nullable = false)
    private Long odometerTrip;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "scooter")
    private Set<TripEntity> trips = new HashSet<>();


    public String getCpuid() {
        return cpuid;
    }

    public void setCpuid(String cpuid) {
        this.cpuid = cpuid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getDebug() {
        return debug;
    }

    public void setDebug(Boolean debug) {
        this.debug = debug;
    }

    public Boolean getConnected() {
        return connected;
    }

    public void setConnected(Boolean connected) {
        this.connected = connected;
    }

    public Boolean getStolen() {
        return stolen;
    }

    public void setStolen(Boolean stolen) {
        this.stolen = stolen;
    }

    public Long getWheelRadius() {
        return wheelRadius;
    }

    public void setWheelRadius(Long wheelRadius) {
        this.wheelRadius = wheelRadius;
    }

    public Long getMinVoltage() {
        return minVoltage;
    }

    public void setMinVoltage(Long minVoltage) {
        this.minVoltage = minVoltage;
    }

    public Long getMaxVoltage() {
        return maxVoltage;
    }

    public void setMaxVoltage(Long maxVoltage) {
        this.maxVoltage = maxVoltage;
    }

    public Long getOdometer() {
        return odometer;
    }

    public void setOdometer(Long odometer) {
        this.odometer = odometer;
    }

    public Long getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(Long batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    public Boolean getPowerEnable() {
        return powerEnable;
    }

    public void setPowerEnable(Boolean powerEnable) {
        this.powerEnable = powerEnable;
    }

    public Long getHornTimer() {
        return hornTimer;
    }

    public void setHornTimer(Long hornTimer) {
        this.hornTimer = hornTimer;
    }

    public Long getHeadLight() {
        return headLight;
    }

    public void setHeadLight(Long headLight) {
        this.headLight = headLight;
    }

    public Long getLastSpeed() {
        return lastSpeed;
    }

    public void setLastSpeed(Long lastSpeed) {
        this.lastSpeed = lastSpeed;
    }

    public Point getLastLocation() {
        return lastLocation;
    }

    public void setLastLocation(Point lastLocation) {
        this.lastLocation = lastLocation;
    }

    public Long getOdometerTrip() {
        return odometerTrip;
    }

    public void setOdometerTrip(Long odometerTrip) {
        this.odometerTrip = odometerTrip;
    }

    public Set<TripEntity> getTrips() {
        return trips;
    }

    public void setTrips(Set<TripEntity> trips) {
        this.trips = trips;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ScooterEntity that = (ScooterEntity) o;

        return cpuid != null ? cpuid.equals(that.cpuid) : that.cpuid == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (cpuid != null ? cpuid.hashCode() : 0);
        return result;
    }
}
