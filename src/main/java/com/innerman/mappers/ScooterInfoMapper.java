package com.innerman.mappers;

import com.innerman.dto.ScooterInfoDTO;
import com.innerman.entity.ScooterEntity;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * Created by petrpopov on 16/09/16.
 */

@Mapper(uses = {TripAggMapper.class})
public interface ScooterInfoMapper {

    @Mappings({
            @Mapping(source = "connected", target = "online"),
            @Mapping(target = "lastTripDuration", source = "trips", qualifiedBy = {TripConverter.class, LastTripConverter.class}),
            @Mapping(target = "totalTime", source = "trips", qualifiedBy = {TripConverter.class, TotalTimeConverter.class}),
            @Mapping(target = "tripCount", expression = "java(scooter.getTrips().size())")
    })
    @BeanMapping(resultType = ScooterInfoDTO.class)
    ScooterInfoDTO toScooterInfoDTO(ScooterEntity scooter);
}
