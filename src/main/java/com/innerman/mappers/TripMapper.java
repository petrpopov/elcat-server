package com.innerman.mappers;

import com.innerman.dto.TripDTO;
import com.innerman.entity.TripEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * Created by petrpopov on 16/09/16.
 */

@Mapper
public interface TripMapper {

    @Mappings({
            @Mapping(source = "scooter.id", target = "scooterId"),
            @Mapping(source = "start", target = "startedAt", dateFormat = "dd.MM.yyyy hh:mm:ss"),
            @Mapping(source = "finish", target = "finishedAt", dateFormat = "dd.MM.yyyy hh:mm:ss")
    })
    TripDTO toTripDto(TripEntity trip);
}
