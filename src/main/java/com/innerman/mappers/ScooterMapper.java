package com.innerman.mappers;

import com.innerman.dto.ScooterDTO;
import com.innerman.entity.ScooterEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * Created by petrpopov on 08/09/16.
 */

@Mapper
public interface ScooterMapper {

    @Mappings(value = {
            @Mapping(source = "connected", target = "online")
    })
    ScooterDTO toScooterDTO(ScooterEntity scooter);
}
