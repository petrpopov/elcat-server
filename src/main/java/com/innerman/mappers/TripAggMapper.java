package com.innerman.mappers;

import com.innerman.entity.TripEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by petrpopov on 16/09/16.
 */

@Component
@TripConverter
public class TripAggMapper {

    @LastTripConverter
    @Transactional(propagation = Propagation.REQUIRED)
    public Long getLastTrip(Collection<TripEntity> trips) {
        Optional<TripEntity> trip = trips
                .stream()
                .filter(t -> t.getCompleted().equals(Boolean.TRUE))
                .sorted((c1, c2) -> c2.getFinish()
                        .compareTo(c1.getFinish()))
                .findFirst();

        if(trip.isPresent()) {
            return trip.get().getDuration();
        }
        return 0L;
    }

    @TotalTimeConverter
    @Transactional(propagation = Propagation.REQUIRED)
    public Long totalTime(Collection<TripEntity> trips) {
        return trips.stream().mapToLong(TripEntity::getDuration).sum();
    }
}
