package com.innerman.config;

import com.innerman.messaging.FromOperationMessageReceiver;
import com.innerman.messaging.FromScooterMessageReceiver;
import com.innerman.messaging.ToOperationMessageReceiver;
import com.innerman.messaging.ToScooterMessageReceiver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.Environment;
import reactor.bus.EventBus;
import reactor.spring.context.config.EnableReactor;

import javax.annotation.PostConstruct;

import static reactor.bus.selector.Selectors.$;

/**
 * Created by petrpopov on 06/09/16.
 */

@Configuration
@EnableReactor
public class ReactorConfiguration {

    @Value("${reactor.from.scooter.queue}")
    private String fromScooterQueue;

    @Value("${reactor.from.operation.queue}")
    private String fromOperationQueue;

    @Value("${reactor.to.scooter.queue}")
    private String toScooterQueue;

    @Value("${reactor.to.operation.queue}")
    private String toOperationQueue;


    @Autowired
    private EventBus eventBus;


    @Bean
    public Environment env() {
        return Environment.initializeIfEmpty()
                .assignErrorJournal();
    }

    @Bean
    public EventBus createEventBus(Environment env) {
        return EventBus.create(env, Environment.THREAD_POOL);
    }

    @Bean
    public FromScooterMessageReceiver fromScooterMessageReceiver() {
        return new FromScooterMessageReceiver();
    }

    @Bean
    public FromOperationMessageReceiver fromOperationMessageReceiver() {
        return new FromOperationMessageReceiver();
    }

    @Bean
    public ToOperationMessageReceiver toOperationMessageReceiver() {
        return new ToOperationMessageReceiver();
    }

    @Bean
    public ToScooterMessageReceiver toScooterMessageReceiver() {
        return new ToScooterMessageReceiver();
    }

    @PostConstruct
    public void init() {
        eventBus.on($(fromScooterQueue), fromScooterMessageReceiver());
        eventBus.on($(fromOperationQueue), fromOperationMessageReceiver());
        eventBus.on($(toOperationQueue), toOperationMessageReceiver());
        eventBus.on($(toScooterQueue), toScooterMessageReceiver());
    }
}
