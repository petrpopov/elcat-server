package com.innerman.utils;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by petrpopov on 08.11.14.
 */
public class DateUtils {

    public static Date getNowUTC() {
        LocalDateTime d = LocalDateTime.now(Clock.systemUTC());
        Instant instant = d.toInstant(ZoneOffset.UTC);
        return Date.from(instant);
    }

    public static LocalDateTime fromDate(Date date) {
        Instant instant = Instant.ofEpochMilli(date.getTime());
        return LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
    }

    public static long getDateDifferenceInSeconds(Date from, Date to) {

        LocalDateTime f = fromDate(from);
        LocalDateTime t = fromDate(to);

        if(f.isBefore(t)) {
            return Duration.between(f, t).toMillis() / 1000;
        }
        else {
            return Duration.between(t, f).toMillis() / 1000;
        }
    }

    public static Date getDatePlusYears(Date date, int years) {

        LocalDateTime f = fromDate(date);
        f = f.plusYears(years);

        Instant instant = f.toInstant(ZoneOffset.UTC);
        return Date.from(instant);
    }

    public static Date getDatePlusSeconds(Date date, long seconds) {

        LocalDateTime f = fromDate(date);
        f = f.plusSeconds(seconds);

        Instant instant = f.toInstant(ZoneOffset.UTC);
        return Date.from(instant);
    }

    public static Calendar dateToCalendar(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    public static XMLGregorianCalendar dateToXMLGregorianCalendar(Date date) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        try {
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Date dateFromString(String value, String format) {

        if(format == null) {
            return null;
        }

        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            return sdf.parse(value);
        } catch (ParseException e) {
            return null;
        }
    }

    public static String dateToSOAPSerialize(Date date) {

        if(date == null) {
            return "";
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String val = sdf.format(date);
        val += ".000Z";
        return val;
    }

    public static String dateToString(Date date, String format) {

        if(date == null) {
            return "";
        }

        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }
}
