package com.innerman.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.innerman.operations.GpsOperation;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;

/**
 * Created by petrpopov on 23/09/2016.
 */
public class GpsUtil {

    private static ObjectMapper mapper = new ObjectMapper();

    public static GpsOperation fromStringToGpsOperation(String str) {
        try {
            GpsOperation op = mapper.readValue(str.trim(), GpsOperation.class);
            return op;
        } catch (Exception e) {
            return null;
        }
    }

    public static Point fromGpsOperationToPoint(GpsOperation op) {

        Geometry geom = wktToGeometry(fromGpsOperationToWktString(op));
        if(geom == null) {
            return null;
        }
        return (Point) geom;
    }

    public static String fromGpsOperationToWktString(GpsOperation op) {
        return "POINT(" + op.getLon().toString() + " " + op.getLat().toString() + ")";
    }

    private static Geometry wktToGeometry(String wktPoint) {
        WKTReader fromText = new WKTReader();
        Geometry geom = null;
        try {
            geom = fromText.read(wktPoint);
        } catch (ParseException e) {
            return null;
        }
        return geom;
    }
}
