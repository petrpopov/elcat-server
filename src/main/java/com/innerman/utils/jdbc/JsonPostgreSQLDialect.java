package com.innerman.utils.jdbc;

import org.hibernate.spatial.dialect.postgis.PostgisDialect;

import java.sql.Types;

/**
 * Created by petrpopov on 15.09.15.
 */
public class JsonPostgreSQLDialect extends PostgisDialect {

    public JsonPostgreSQLDialect() {
        super();
        this.registerColumnType(Types.JAVA_OBJECT, "jsonb");
    }
}
