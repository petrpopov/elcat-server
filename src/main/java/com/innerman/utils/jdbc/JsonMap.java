package com.innerman.utils.jdbc;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by petrpopov on 15.09.15.
 */
public class JsonMap implements Serializable {

    private static final long serialVersionUID    = 1L;

    private Map json;

    public JsonMap() {
    }

    public JsonMap(Map json) {
        this.json = json;
    }

    public Map getJson() {
        return json;
    }

    public void setJson(Map json) {
        this.json = json;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JsonMap jsonMap = (JsonMap) o;

        return !(json != null ? !json.equals(jsonMap.json) : jsonMap.json != null);

    }

    @Override
    public int hashCode() {
        return json != null ? json.hashCode() : 0;
    }
}
