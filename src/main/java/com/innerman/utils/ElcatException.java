package com.innerman.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by petrpopov on 19.08.15.
 */
public class ElcatException extends Exception {

    private Boolean unknownUsers = false;
    private List<String> emails;

    public ElcatException() {
    }

    public ElcatException(String message) {
        super(message);
    }

    public ElcatException(String message, List<String> emails) {
        super(message);
        this.emails = emails;
        unknownUsers = true;
    }

    public ElcatException(String message, String email) {
        super(message);

        emails = new ArrayList<>();
        emails.add(email);
        unknownUsers = true;
    }

    public ElcatException(String message, Throwable cause) {
        super(message, cause);
    }

    public ElcatException(Throwable cause) {
        super(cause);
    }

    public ElcatException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public Boolean getUnknownUsers() {
        return unknownUsers;
    }

    public void setUnknownUsers(Boolean unknownUsers) {
        this.unknownUsers = unknownUsers;
    }

    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }
}
