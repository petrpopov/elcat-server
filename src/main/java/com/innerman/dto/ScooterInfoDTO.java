package com.innerman.dto;

import java.io.Serializable;

/**
 * Created by petrpopov on 16/09/16.
 */
public class ScooterInfoDTO extends ScooterDTO implements Serializable {

    private Integer tripCount;
    private Long lastTripDuration;
    private Long totalTime;

    public Integer getTripCount() {
        return tripCount;
    }

    public void setTripCount(Integer tripCount) {
        this.tripCount = tripCount;
    }

    public Long getLastTripDuration() {
        return lastTripDuration;
    }

    public void setLastTripDuration(Long lastTripDuration) {
        this.lastTripDuration = lastTripDuration;
    }

    public Long getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(Long totalTime) {
        this.totalTime = totalTime;
    }
}
