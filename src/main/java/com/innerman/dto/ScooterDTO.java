package com.innerman.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * Created by petrpopov on 08/09/16.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ScooterDTO implements Serializable {

    private Long id;
    private String cpuid;
    private String title;
    private Boolean active;
    private Boolean debug;
    private Boolean online;
    private Boolean stolen;
    private Long wheelRadius;
    private Long minVoltage;
    private Long maxVoltage;
    private Long odometer;
    private Long batteryLevel;
    private Boolean powerEnable;
    private Long hornTimer;
    private Long headLight;
    private Long lastSpeed;
    private Long odometerTrip;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCpuid() {
        return cpuid;
    }

    public void setCpuid(String cpuid) {
        this.cpuid = cpuid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getDebug() {
        return debug;
    }

    public void setDebug(Boolean debug) {
        this.debug = debug;
    }

    public Boolean getOnline() {
        return online;
    }

    public void setOnline(Boolean online) {
        this.online = online;
    }

    public Boolean getStolen() {
        return stolen;
    }

    public void setStolen(Boolean stolen) {
        this.stolen = stolen;
    }

    public Long getWheelRadius() {
        return wheelRadius;
    }

    public void setWheelRadius(Long wheelRadius) {
        this.wheelRadius = wheelRadius;
    }

    public Long getMinVoltage() {
        return minVoltage;
    }

    public void setMinVoltage(Long minVoltage) {
        this.minVoltage = minVoltage;
    }

    public Long getMaxVoltage() {
        return maxVoltage;
    }

    public void setMaxVoltage(Long maxVoltage) {
        this.maxVoltage = maxVoltage;
    }

    public Long getOdometer() {
        return odometer;
    }

    public void setOdometer(Long odometer) {
        this.odometer = odometer;
    }

    public Long getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(Long batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    public Boolean getPowerEnable() {
        return powerEnable;
    }

    public void setPowerEnable(Boolean powerEnable) {
        this.powerEnable = powerEnable;
    }

    public Long getHornTimer() {
        return hornTimer;
    }

    public void setHornTimer(Long hornTimer) {
        this.hornTimer = hornTimer;
    }

    public Long getHeadLight() {
        return headLight;
    }

    public void setHeadLight(Long headLight) {
        this.headLight = headLight;
    }

    public Long getLastSpeed() {
        return lastSpeed;
    }

    public void setLastSpeed(Long lastSpeed) {
        this.lastSpeed = lastSpeed;
    }

    public Long getOdometerTrip() {
        return odometerTrip;
    }

    public void setOdometerTrip(Long odometerTrip) {
        this.odometerTrip = odometerTrip;
    }
}
