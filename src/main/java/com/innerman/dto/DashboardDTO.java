package com.innerman.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * Created by petrpopov on 24/09/2016.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class DashboardDTO implements Serializable {

    private Long scooters;
    private Long users;
    private Long driveMinutes;

    public Long getScooters() {
        return scooters;
    }

    public void setScooters(Long scooters) {
        this.scooters = scooters;
    }

    public Long getUsers() {
        return users;
    }

    public void setUsers(Long users) {
        this.users = users;
    }

    public Long getDriveMinutes() {
        return driveMinutes;
    }

    public void setDriveMinutes(Long driveMinutes) {
        this.driveMinutes = driveMinutes;
    }
}
