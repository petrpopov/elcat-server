/**
 * Created by petrpopov on 22/09/16.
 */
import React from 'react';
import NavLink from './NavLink';
import ScooterActions from '../actions/ScooterActions';
import { browserHistory } from 'react-router'

export default React.createClass({

    saveScooter: function() {
        ScooterActions.saveScooter(this.props.scooter, this.props.scooterId, this.afterSave);
    },

    afterSave: function() {
        browserHistory.push('/scooters')
    },

    handleChange: function(key) {
        return function (e) {
            this.props.scooter[key] = e.target.value;
            this.setState(this.props.scooter);
        }.bind(this);
    },

    render: function() {
        return (
            <form className="col-lg-6">
                <div className="form-group">
                    <label htmlFor="titleInput">Title</label>
                    <input type="text" className="form-control"
                           id="titleInput" placeholder="My scooter"
                           defaultValue={this.props.scooter.title} onChange={this.handleChange('title')}/>
                </div>
                <div className="form-group">
                    <label htmlFor="raidusInput">Wheel Radius</label>
                    <input type="text" className="form-control"
                           id="raidusInput" placeholder="600"
                           defaultValue={this.props.scooter.wheelRadius} onChange={this.handleChange('wheelRadius')}/>
                </div>
                <div className="form-group">
                    <label htmlFor="timerInput">Horn Timer</label>
                    <input type="text" className="form-control"
                           id="timerInput" placeholder="2"
                           defaultValue={this.props.scooter.hornTimer} onChange={this.handleChange('hornTimer')}/>
                </div>
                <div className="form-group">
                    <label htmlFor="headLightInput">Head Light</label>
                    <input type="text" className="form-control"
                           id="headLightInput" placeholder="1"
                           defaultValue={this.props.scooter.headLight} onChange={this.handleChange('headLight')} />
                </div>
                <div className="form-group">
                    <label htmlFor="debug1">Debug</label>
                    <label className="radio-inline">
                        <input type="radio" name="debug" id="debug1" value="true"
                               defaultChecked={this.props.scooter.debug} onChange={this.handleChange('debug')}/> TRUE
                    </label>
                    <label className="radio-inline">
                        <input type="radio" name="debug" id="debug2" value="false"
                               defaultChecked={!this.props.scooter.debug} onChange={this.handleChange('debug')}/> FALSE
                    </label>
                </div>
                <div className="form-group">
                    <label htmlFor="stolen1">Stolen</label>
                    <label className="radio-inline">
                        <input type="radio" name="debug" id="stolen1" value="true"
                               defaultChecked={this.props.scooter.stolen} onChange={this.handleChange('stolen')}/> TRUE
                    </label>
                    <label className="radio-inline">
                        <input type="radio" name="debug" id="stolen2" value="false"
                               defaultChecked={!this.props.scooter.stolen} onChange={this.handleChange('stolen')}/> FALSE
                    </label>
                </div>
                <button type="button" className="btn btn-primary" onClick={this.saveScooter}>Save</button>
                <NavLink to={'/scooters/' + this.props.scooterId}>
                    <button type="button" className="btn btn-default">Cancel</button>
                </NavLink>
            </form>
        );
    }
});