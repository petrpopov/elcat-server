/**
 * Created by petrpopov on 15/09/16.
 */
import React from'react';
import TripBox from './TripsBox'
import bootstrap from 'bootstrap'
import NavLink from './NavLink';

export default React.createClass({

    render: function() {
        return(
            <div className="col-lg-12">
                <div role="tabpanel" className="panel panel-transparent">
                    <ul role="tablist" className="nav nav-tabs nav-justified">
                        <li role="presentation" className="active">
                            <a href="#stuffpanel" aria-controls="home" role="tab" data-toggle="tab" className="bb0">
                                <em className="icon-speedometer fa-fw"></em>Stuff Panel</a>
                        </li>
                        <li role="presentation">
                            <a href="#tripspanel" aria-controls="profile" role="tab" data-toggle="tab" className="bb0">
                                <em className="fa fa-space-shuttle fa-fw"></em>Trips Panel</a>
                        </li>
                    </ul>
                    <div className="tab-content p0 bg-white">
                        <div id="stuffpanel" role="tabpanel" className="tab-pane active">
                            <div className="list-group mb0">
                                <span className="list-group-item">
                                        <span className="label label-purple pull-right">{this.props.scooter.title}</span>
                                        <em className="fa fa-fw fa-internet-explorer mr"></em>Title
                                </span>
                                <span className="list-group-item">
                                        <span className="label label-green pull-right">{this.props.scooter.online}</span>
                                        <em className="fa fa-fw fa-internet-explorer mr"></em>Connected
                                </span>
                                <span className="list-group-item">
                                        <span className="label label-green pull-right">{this.props.scooter.stolen}</span>
                                        <em className="fa fa-fw fa-exclamation-circle mr"></em>Stolen
                                </span>
                                <span className="list-group-item">
                                        <span className="label label-green pull-right">{this.props.scooter.debug}</span>
                                        <em className="fa fa-fw fa-gears mr"></em>Debug
                                </span>
                                <span className="list-group-item">
                                        <span className="label label-purple pull-right">{this.props.scooter.tripCount}</span>
                                        <em className="fa fa-fw fa-space-shuttle mr"></em>Scooter trips
                                </span>
                                <span className="list-group-item">
                                        <span className="label label-purple pull-right">{this.props.scooter.odometer} km</span>
                                        <em className="fa fa-fw icon-speedometer mr"></em>Total Odometer
                                </span>
                                <span className="list-group-item">
                                        <span className="label label-purple pull-right">{this.props.scooter.totalTime} min</span>
                                        <em className="fa fa-fw icon-clock mr"></em>Total Time
                                </span>
                                <span className="list-group-item">
                                        <span className="label label-purple pull-right">{this.props.scooter.wheelRadius}</span>
                                        <em className="fa fa-fw fa-refresh mr"></em>Wheel Radius
                                </span>
                                <span className="list-group-item">
                                        <span className="label label-purple pull-right">{this.props.scooter.hornTimer} sec</span>
                                        <em className="fa fa-fw icon-clock mr"></em>Horn Timer
                                </span>
                                <span className="list-group-item">
                                        <span className="label label-purple pull-right">{this.props.scooter.headLight}</span>
                                        <em className="fa fa-fw fa-warning mr"></em>Headlight
                                </span>
                                <span className="list-group-item">
                                        <span className="label label-purple pull-right">{this.props.scooter.batteryLevel} %</span>
                                        <em className="fa fa-fw fa-warning mr"></em>Battery Level
                                </span>
                                <span className="list-group-item">
                                        <span className="label label-purple pull-right">{this.props.scooter.powerEnable}</span>
                                        <em className="fa fa-fw fa-warning mr"></em>Power Enable
                                </span>
                            </div>
                            <div className="panel-footer text-right">
                                <NavLink to={'/scooters/' + this.props.scooterId + '/edit'} className="btn btn-primary btn-sm">Edit</NavLink>
                            </div>
                        </div>
                        <div id="tripspanel" role="tabpanel" className="tab-pane">
                            <TripBox scooterId={this.props.scooterId} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});