/**
 * Created by petrpopov on 14/09/16.
 */
import React from 'react';
import { Link } from 'react-router';


export default React.createClass({
    linkStyle: {
        color: 'white'
    },

    render: function() {
        return(
            <div className="col-lg-3 col-sm-6">
                <div className="panel widget bg-primary">
                    <div className="row row-table">
                        <div className="col-xs-4 text-center bg-primary-dark pv-lg">
                            <em className="icon-rocket fa-3x"></em>
                        </div>
                        <div className="col-xs-8 pv-lg">
                            <div className="h2 mt0">{this.props.dashboard.scooters}</div>
                            <div className="text-uppercase">
                                <Link style={this.linkStyle} to="/scooters">Scooters</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});