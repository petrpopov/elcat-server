/**
 * Created by petrpopov on 15/09/16.
 */
import React from 'react';
import NavLink from '../NavLink';

export default React.createClass({
    render: function() {
        return (
            <aside className="aside">
                <div className="aside-inner">
                    <nav data-sidebar-anyclick-close="" className="sidebar">
                        <ul className="nav">
                            <li className="has-user-block">
                                <div id="user-block" className="collapse">
                                    <div className="item user-block">
                                        <div className="user-block-picture">
                                            <div className="user-block-status">
                                                <img src="/img/user/02.jpg" alt="Avatar" width="60" height="60" className="img-thumbnail img-circle"/>
                                                <div className="circle circle-success circle-lg"></div>
                                            </div>
                                        </div>
                                        <div className="user-block-info">
                                            <span className="user-block-name">Hello, Mike</span>
                                            <span className="user-block-role">Designer</span>
                                        </div>
                                    </div>
                                </div>
                            </li>


                            <li className="nav-heading ">
                                <span>Main Navigation</span>
                            </li>
                            <li className=" ">
                                <NavLink to="/dashboard" title="Scooters">
                                    <em className="icon-speedometer"></em>
                                    <span>Dashboard</span>
                                </NavLink>
                            </li>
                            <li className=" ">
                                <NavLink href="/scooters" title="Scooters" >
                                    <em className="icon-rocket"></em>
                                    <span>Scooters</span>
                                </NavLink>
                            </li>
                            <li className=" ">
                                <NavLink href="/users" title="Users">
                                    <em className="icon-user"></em>
                                    <span>Users</span>
                                </NavLink>
                            </li>
                        </ul>
                    </nav>
                </div>
            </aside>
        );
    }
});