/**
 * Created by petrpopov on 15/09/16.
 */
import React from 'react';

export default React.createClass({
    render: function() {
        return (
            <aside className="offsidebar hide">
                <nav>
                    <div role="tabpanel">
                        <ul role="tablist" className="nav nav-tabs nav-justified">
                            <li role="presentation" className="active">
                                <a href="#app-settings" aria-controls="app-settings" role="tab" data-toggle="tab">
                                    <em className="icon-equalizer fa-lg"></em>
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#app-chat" aria-controls="app-chat" role="tab" data-toggle="tab">
                                    <em className="icon-user fa-lg"></em>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </aside>
        );
    }
});