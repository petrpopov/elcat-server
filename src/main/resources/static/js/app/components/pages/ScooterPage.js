/**
 * Created by petrpopov on 13/09/16.
 */
import React from'react';
import ScooterWidget from '../ScooterWidget';
import ScooterControlWidget from '../ScooterControlWidget';
import ScooterDataWidget from '../ScooterDataWidget';
import ScooterStore from '../../stores/ScooterStore';
import ScooterActions from '../../actions/ScooterActions';

export default React.createClass({

    //data
    getInitialState: function(){
        return {
            scooter: ScooterStore.get()
        }
    },
    componentDidMount: function(){
        ScooterStore.addChangeListener(this._onChange);
        this.getDataIfNeeded(this.props);
    },
    componentWillUnmount: function(){
        ScooterStore.removeChangeListener(this._onChange);
    },
    _onChange: function(){
        this.setState({
            scooter: ScooterStore.get()
        })
    },
    handleSetItem: function(newItem){
        ScooterActions.setItem(newItem);
    },
    handleRemoveItem: function(item){
        ScooterActions.removeItem(item);
    },
    getDataIfNeeded: function(props) {
        var scooter = ScooterStore.get();
        if(!scooter.hasOwnProperty('id')) {
            console.log('need loading scooter');
            ScooterActions.getScooter(props.params.scooterId);
        }
    },
    //end of data

    render: function() {
        return (
            <div className="row">
                <ScooterWidget scooter={this.state.scooter} scooterId={this.props.params.scooterId}/>
                <ScooterControlWidget scooter={this.state.scooter} scooterId={this.props.params.scooterId}/>
                <ScooterDataWidget scooter={this.state.scooter} scooterId={this.props.params.scooterId}/>
            </div>
        );
    }
});