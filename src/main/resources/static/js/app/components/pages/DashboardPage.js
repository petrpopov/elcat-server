/**
 * Created by petrpopov on 13/09/16.
 */
import React from 'react';
import WidgetPanel from '../WidgetPanel';
import ScootersPanel from '../ScootersPanel';
import DashboardActions from '../../actions/DashboardActions';
import DashboardStore from '../../stores/DashboardStore';

export default React.createClass({

    //data
    getInitialState: function(){
        return {
            dashboard: DashboardStore.get()
        }
    },
    componentDidMount: function(){
        DashboardStore.addChangeListener(this._onChange);
        this.getDataIfNeeded(this.props);
    },
    componentWillUnmount: function(){
        DashboardStore.removeChangeListener(this._onChange);
    },
    _onChange: function(){
        this.setState({
            dashboard: DashboardStore.get()
        })
    },
    handleSetItem: function(newItem){
        DashboardActions.setItem(newItem);
    },
    handleRemoveItem: function(item){
        DashboardActions.removeItem(item);
    },
    getDataIfNeeded: function(props) {
        var dashboard = DashboardStore.get();
        if(!dashboard.hasOwnProperty('scooters')) {
            console.log('need loading dashboard data');
            DashboardActions.getDashboardInfo();
        }
    },
    //end of data

    render: function() {
        return (
            <div className="row">
                <WidgetPanel dashboard={this.state.dashboard} />
                <ScootersPanel />
            </div>
        );
    }
});

