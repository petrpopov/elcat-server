/**
 * Created by petrpopov on 13/09/16.
 */
import React from'react';
import {Link} from 'react-router';
import ScootersPanel from '../ScootersPanel';

export default React.createClass({
    render: function() {
        return (
            <div className="row">
                <ScootersPanel title="Scooters" />
            </div>
        );
    }
});

