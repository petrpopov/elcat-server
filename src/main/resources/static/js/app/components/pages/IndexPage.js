/**
 * Created by petrpopov on 13/09/16.
 */
import React from 'react';
import NavLink from '../NavLink';
import TopNavbar from '../TopNavbar'
import Sidebar from './Sidebar'
import SideUserbar from './SideUserbar'


export default React.createClass({

    render: function() {
        return(
            <div className="wrapper">
                <TopNavbar/>

                <Sidebar/>

                <SideUserbar/>

                <section>
                    <div id="contentWrapper" className="content-wrapper">
                        <div className="content-heading">
                            Dashboard
                            <small>Welcome to ELCAT Server</small>
                        </div>

                        {this.props.children}
                    </div>
                </section>

                <footer>
                    <span>&copy; 2016 - <a href="http://www.innermansoftware.com" target="_blank">Innerman Software</a></span>
                </footer>
            </div>
        );
    }
});

