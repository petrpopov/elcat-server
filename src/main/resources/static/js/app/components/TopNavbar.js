/**
 * Created by petrpopov on 15/09/16.
 */
import React from 'react';
import { IndexLink } from 'react-router'

export default React.createClass({
    render: function() {
        return (
            <header className="topnavbar-wrapper">
                <nav role="navigation" className="navbar topnavbar">
                    <div className="navbar-header">
                        <IndexLink to="/" activeClassName="active">
                            <div className="brand-logo">
                                <img src="/img/logo.png" alt="App Logo" className="img-responsive"/>
                            </div>
                            <div className="brand-logo-collapsed">
                                <img src="/img/logo-single.png" alt="App Logo" className="img-responsive"/>
                            </div>
                        </IndexLink>
                    </div>
                    <div className="nav-wrapper">
                        <ul className="nav navbar-nav">
                            <li>
                                <a href="#" data-trigger-resize="" data-toggle-state="aside-collapsed" className="hidden-xs">
                                    <em className="fa fa-navicon"></em>
                                </a>
                                <a href="#" data-toggle-state="aside-toggled" data-no-persist="true" className="visible-xs sidebar-toggle">
                                    <em className="fa fa-navicon"></em>
                                </a>
                            </li>
                            <li>
                                <a id="user-block-toggle" href="#user-block" data-toggle="collapse">
                                    <em className="icon-user"></em>
                                </a>
                            </li>
                        </ul>
                        <ul className="nav navbar-nav navbar-right">
                            <li>
                                <a href="#" data-search-open="">
                                    <em className="icon-magnifier"></em>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <form role="search" action="search.html" className="navbar-form">
                        <div className="form-group has-feedback">
                            <input type="text" placeholder="Type and hit enter ..." className="form-control"/>
                            <div data-search-dismiss="" className="fa fa-times form-control-feedback"></div>
                        </div>
                        <button type="submit" className="hidden btn btn-default">Submit</button>
                    </form>
                </nav>
            </header>
        );
    }
});