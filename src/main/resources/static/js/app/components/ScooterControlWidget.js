/**
 * Created by petrpopov on 16/09/16.
 */
import React from'react';
import DebugModal from './DebugModal';
import StolenModal from './StolenModal';
import PowerModal from './PowerModal';

export default React.createClass({
    render: function() {
        return (
            <div className="col-lg-12">
                <DebugModal scooter={this.props.scooter} scooterId={this.props.scooterId} />
                <StolenModal scooter={this.props.scooter} scooterId={this.props.scooterId} />
                <PowerModal scooter={this.props.scooter} scooterId={this.props.scooterId} />
            </div>
        );
    }
});