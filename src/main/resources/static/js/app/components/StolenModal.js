/**
 * Created by petrpopov on 20/09/16.
 */
import React from 'react';
import ScooterActions from '../actions/ScooterActions';

export default React.createClass({

    toggleStolen: function() {
        ScooterActions.toggleStolen(this.props.scooter, this.props.scooterId);
    },

    render: function() {
        return(
            <div className="col-lg-3">
                <div className="panel widget">
                    <div className="row row-table row-flush">
                        <div className="col-sm-4 text-center">
                            <button className="btn btn-primary btn-square" type="button" onClick={this.toggleStolen}>
                                {this.props.scooter['stolen'] === true ? 'DISABLE' : 'ENABLE'}
                            </button>
                        </div>
                        <div className="col-sm-8">
                            <div className="panel-body text-center">
                                <h4 className="mt0">STOLEN MODE</h4>
                                <p className="mb0 text-muted">{this.props.scooter['stolen'] === true ? 'ENABLED' : 'DISABLED'}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});