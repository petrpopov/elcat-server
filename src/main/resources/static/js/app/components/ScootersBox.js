/**
 * Created by petrpopov on 14/09/16.
 */
import React from 'react';
import Reactable from 'reactable';
import NavLink from './NavLink';
import request from 'superagent';


export default React.createClass({
    render: function() {
        var Table = Reactable.Table,
            Tr = Reactable.Tr,
            Td = Reactable.Td;

        const columns = [
            {key: 'id', label: 'ID'},
            {key: 'cpuid', label: 'CPUID'},
            {key: 'title', label: 'Title'},
            {key: 'online', label: 'Online'},
            {key: 'lastSpeed', label: 'Speed, km/h'},
            {key: 'odometer', label: 'Odometer, m'},
            {key: 'odometerTrip', label: 'Trip, m'},
            {key: 'batteryLevel', label: 'Battery'},
            {key: 'headLight', label: 'Headlight'},
            {key: 'hornTimer', label: 'Horn Timer'},
            {key: 'powerEnable', label: 'Power Enable'},
            {key: 'stolen', label: 'Stolen'},
            {key: 'debug', label: 'Debug'}
        ];

        var data = this.state.scooters;

        return (
            <div className="table-responsive">
                <Table className="table table-condensed table-bordered table-hover"
                       columns={columns}
                       itemsPerPage={20} pageButtonLimit={7}>
                    {data.map(function(item, i) {
                        var link = "/scooters/" + item.id;
                        var cpuidlink = "/scooters/" + item.cpuid;
                            return <Tr key={i}>
                                <Td column="id">
                                    <NavLink to={link}>{item.id}</NavLink>
                                </Td>
                                <Td column="cpuid">
                                    <NavLink to={cpuidlink}>{item.cpuid}</NavLink>
                                </Td>
                                <Td column="title">{item.title}</Td>
                                <Td column="online">{item.online}</Td>
                                <Td column="lastSpeed">{item.lastSpeed}</Td>
                                <Td column="odometer">{item.odometer}</Td>
                                <Td column="odometerTrip">{item.odometerTrip}</Td>
                                <Td column="batteryLevel">{item.batteryLevel}</Td>
                                <Td column="headLight">{item.headLight}</Td>
                                <Td column="hornTimer">{item.hornTimer}</Td>
                                <Td column="powerEnable">{item.powerEnable}</Td>
                                <Td column="stolen">{item.stolen}</Td>
                                <Td column="debug">{item.debug}</Td>
                            </Tr>
                        })
                    }
                </Table>
            </div>
        );
    },

    getInitialState: function() {
        return {scooters: []};
    },

    componentDidMount: function() {
        var that = this;

        request
            .get('/api/scooters')
            .end(function(err, res) {
                var data = JSON.parse(res.text);

                if(!data || !data.result) {
                    return;
                }

                that.setState({scooters: data.result});
            });
    }
});