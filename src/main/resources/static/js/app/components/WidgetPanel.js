/**
 * Created by petrpopov on 14/09/16.
 */
import React from 'react';
import ScootersWidget from './ScootersWidget'
import UsersWidget from './UsersWidget'
import DriveWidget from './DriveWidget'
import TimeWidget from './TimeWidget'

export default React.createClass({
    render: function() {
        return (
            <div className="row">
                <ScootersWidget dashboard={this.props.dashboard} />
                <UsersWidget dashboard={this.props.dashboard}/>
                <DriveWidget dashboard={this.props.dashboard}/>
                <TimeWidget date={new Date().getTime()}/>
            </div>
        );
    }
});