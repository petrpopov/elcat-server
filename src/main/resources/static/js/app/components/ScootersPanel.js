/**
 * Created by petrpopov on 14/09/16.
 */

import React from 'react';
import { Link } from 'react-router';
import ScootersBox from './ScootersBox'

export default React.createClass({
    render: function() {
        return (
            <div className="panel panel-default">
                <div className="panel-heading">
                    <Link to="/scooters">Scooters</Link>
                </div>
                <div className="panel-body">
                    <ScootersBox />
                </div>
            </div>
        );
    }
});

