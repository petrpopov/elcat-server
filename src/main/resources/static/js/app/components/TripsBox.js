/**
 * Created by petrpopov on 16/09/16.
 */
import React from 'react';
import Reactable from 'reactable';
import NavLink from './NavLink'

export default React.createClass({
    render: function() {
        var Table = Reactable.Table,
            Tr = Reactable.Tr,
            Td = Reactable.Td;

        const columns = [
            {key: 'id', label: 'ID'},
            {key: 'number', label: 'Number'},
            {key: 'startedAt', label: 'Started At'},
            {key: 'finishedAt', label: 'Finished At'},
            {key: 'duration', label: 'duration'},
            {key: 'completed', label: 'completed'}
        ];

        var that = this;
        var data = this.state.trips;

        return (
            <div className="table-responsive">
                <Table className="table table-condensed table-bordered table-hover"
                       columns={columns}
                       itemsPerPage={20} pageButtonLimit={7}>
                    {
                        data.map(function(item, i) {
                            var link = "/scooter/" + that.props.scooterId + "/trips/" + item.id;
                            return <Tr key={i}>
                                <Td column="id">
                                    <NavLink to={link}>{item.id}</NavLink>
                                </Td>
                                <Td column="number">{item.number}</Td>
                                <Td column="startedAt">{item.startedAt}</Td>
                                <Td column="finishedAt">{item.finishedAt}</Td>
                                <Td column="duration">{item.duration}</Td>
                                <Td column="completed">{item.completed}</Td>
                            </Tr>
                        })
                    }
                </Table>
            </div>
        );
    },

    getInitialState: function() {
        return {trips: []};
    },

    componentDidMount: function() {
        var that = this;

        $.ajax({
            url: '/api/scooters/' + that.props.scooterId + "/trips",
            method: 'GET',
            success: function(data) {
                if(!data || !data.result) {
                    return;
                }

                that.setState({trips: data.result});
            }
        })
    }
});