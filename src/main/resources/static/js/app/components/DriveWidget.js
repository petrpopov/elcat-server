/**
 * Created by petrpopov on 14/09/16.
 */
import React from 'react';

export default React.createClass({
    render: function() {
        return (
            <div className="col-lg-3 col-md-6 col-sm-12">
                <div className="panel widget bg-green">
                    <div className="row row-table">
                        <div className="col-xs-4 text-center bg-green-dark pv-lg">
                            <em className="icon-speedometer fa-3x"></em>
                        </div>
                        <div className="col-xs-8 pv-lg">
                            <div className="h2 mt0">{this.props.dashboard.driveMinutes}</div>
                            <div className="text-uppercase">Completed mins</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});