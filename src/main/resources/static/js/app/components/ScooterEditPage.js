/**
 * Created by petrpopov on 22/09/16.
 */
import React from 'react';
import ScooterEditForm from './ScooterEditForm';
import ScooterStore from '../stores/ScooterStore';
import ScooterActions from '../actions/ScooterActions';

export default React.createClass({

    getInitialState: function() {
        return {
            scooter: ScooterStore.get()
        }
    },
    componentDidMount: function(){
        ScooterStore.addChangeListener(this._onChange);
        this.getDataIfNeeded(this.props);
    },
    componentWillUnmount: function(){
        ScooterStore.removeChangeListener(this._onChange);
    },
    _onChange: function(){
        this.setState({
            scooter: ScooterStore.get()
        })
    },
    handleSetItem: function(newItem){
        ScooterActions.setItem(newItem);
    },
    handleRemoveItem: function(item){
        ScooterActions.removeItem(item);
    },
    getDataIfNeeded: function(props) {
        var scooter = ScooterStore.get();
        if(!scooter.hasOwnProperty('id')) {
            console.log('need loading scooter for edit');
            ScooterActions.getScooter(props.params.scooterId);
        }
    },

    render: function() {
        return (
            <div className="row">
                <h2>Edit Scooter: {this.props.params.scooterId}</h2>
                <ScooterEditForm scooter={this.state.scooter} scooterId={this.props.params.scooterId} />
            </div>
        );
    }
});