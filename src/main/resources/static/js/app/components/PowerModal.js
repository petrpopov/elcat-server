/**
 * Created by petrpopov on 06/10/2016.
 */
import React from 'react';
import ScooterActions from '../actions/ScooterActions';

export default React.createClass({

    togglePower: function() {

    },

    render: function() {
        return (
            <div className="col-lg-3">
                <div className="panel widget">
                    <div className="row row-table row-flush">
                        <div className="col-sm-4 text-center">
                            <button className="btn btn-primary btn-square" type="button" onClick={this.toggleDebug}>
                                {this.props.scooter.powerEnable === true ? 'DISABLE' : 'ENABLE'}
                            </button>
                        </div>
                        <div className="col-sm-8">
                            <div className="panel-body text-center">
                                <h4 className="mt0">DEBUG MODE</h4>
                                <p className="mb0 text-muted">{this.props.scooter.powerEnable === true ? 'ENABLED' : 'DISABLED'}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});