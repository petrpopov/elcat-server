/**
 * Created by petrpopov on 14/09/16.
 */
import React from 'react'
import { Link } from 'react-router'

export default React.createClass({
    activeLinkStyle: {
        'color': '#515253'
    },

    render: function() {
        return <Link {...this.props} activeStyle={this.activeLinkStyle}/>
    }
});