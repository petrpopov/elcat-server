/**
 * Created by petrpopov on 14/09/16.
 */
import React from 'react';
import moment from 'moment'

export default React.createClass({

    getTime: function(date, format) {
        return moment( date ).format(format);
    },

    render: function() {

        var datetime = this.props.date;
        if(!datetime) {
            datetime = new Date();
        }

        return (
            <div className="col-lg-3 col-md-6 col-sm-12">
                <div className="panel widget">
                    <div className="row row-table">
                        <div className="col-xs-4 text-center bg-green pv-lg">
                            <div data-now="" data-format="MMMM" className="text-sm">{this.getTime(datetime, 'MMMM')}</div>
                            <br/>
                            <div data-now="" data-format="D" className="h2 mt0">{this.getTime(datetime, 'D')}</div>
                        </div>
                        <div className="col-xs-8 pv-lg">
                            <div data-now="" data-format="dddd" className="text-uppercase">{this.getTime(datetime, 'dddd')}</div>
                            <br/>
                            <div data-now="" data-format="h:mm" className="h2 mt0">{this.getTime(datetime, 'h:mm')}</div>
                            <div data-now="" data-format="ss" className="h2 mt0">{this.getTime(datetime, ':ss')}</div>
                            <div data-now="" data-format="a" className="text-muted text-sm">{this.getTime(datetime, 'a')}</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});