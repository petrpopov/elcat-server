/**
 * Created by petrpopov on 15/09/16.
 */
import React from'react';

export default React.createClass({
    render: function() {
        return(
            <div className="col-lg-12">
                <div className="col-lg-3 col-sm-6">
                    <div className="panel bg-info-light pt b0 widget">
                        <div className="ph">
                            <em className="fa fa-space-shuttle fa-lg pull-right"></em>
                            <div className="h2 mt0">{this.props.scooter.tripCount}</div>
                            <div className="text-uppercase">Trips</div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 col-sm-6">
                    <div className="panel widget bg-purple-light pt b0 widget">
                        <div className="ph">
                            <em className="fa fa-warning fa-lg pull-right"></em>
                            <div className="h2 mt0">{this.props.scooter.batteryLevel}
                                <span className="text-sm text-white">%</span>
                            </div>
                            <div className="text-uppercase">Battery Level</div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 col-md-6 col-sm-12">
                    <div className="panel widget bg-purple-light pt b0 widget">
                        <div className="ph">
                            <em className="fa fa-motorcycle fa-lg pull-right"></em>
                            <div className="h2 mt0">{this.props.scooter.lastSpeed}
                                <span className="text-sm text-white">km/h</span></div>
                            <div className="text-uppercase">Speed</div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 col-md-6 col-sm-12">
                    <div className="panel widget bg-purple-light pt b0 widget">
                        <div className="ph">
                            <em className="icon-clock fa-lg pull-right"></em>
                            <div className="h2 mt0">{this.props.scooter.lastTripDuration}
                                <span className="text-sm text-white">min</span></div>
                            <div className="text-uppercase">Last trip time</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    },
});