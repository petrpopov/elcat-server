/**
 * Created by petrpopov on 14/09/16.
 */
import React from 'react';

export default React.createClass({
    render: function() {
        return (
            <div className="col-lg-3 col-sm-6">
                <div className="panel widget bg-purple">
                    <div className="row row-table">
                        <div className="col-xs-4 text-center bg-purple-dark pv-lg">
                            <em className="icon-user fa-3x"></em>
                        </div>
                        <div className="col-xs-8 pv-lg">
                            <div className="h2 mt0">{this.props.dashboard.users}
                            </div>
                            <div className="text-uppercase">Users</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});