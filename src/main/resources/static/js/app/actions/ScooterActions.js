import AppDispatcher from '../dispatcher/AppDispatcher';
import AppConstants from '../constants/AppConstants';
import request from 'superagent';

var ScooterActions = {

    setItem: function(item){
        AppDispatcher.handleAction({
            actionType: AppConstants.SET_SCOOTER,
            data: item
        });
    },

    removeItem: function(item){
        AppDispatcher.handleAction({
            actionType: AppConstants.REMOVE_SCOOTER,
            data: item
        })
    },

    getScooter: function(scooterId) {
        request
            .get('/api/scooters/' + scooterId)
            .end(function(err, res) {
                var data = JSON.parse(res.text);

                AppDispatcher.handleAction({
                    actionType: AppConstants.SET_SCOOTER,
                    data: data.result
                });
            });
    },

    toggleDebug: function(scooter, scooterId) {
        console.log('changing debug state for scooter ' + scooterId);
        var curDebugState = scooter.debug;

        request.post('/api/scooters/' + scooterId + '/command')
            .set('Content-Type', 'application/json')
            .send({commandName: 'debug', commandValue: !curDebugState})
            .end(function(err, res) {
                console.log('changed scooter stolen mode ' + scooterId);
                scooter.debug = !curDebugState;

                AppDispatcher.handleAction({
                    actionType: AppConstants.SET_SCOOTER,
                    data: scooter
                });
            });
    },

    toggleStolen: function(scooter, scooterId) {
        console.log('changing stolen state for scooter ' + scooterId);
        var curStolenState = scooter.stolen;

        request.post('/api/scooters/' + scooterId + '/command')
            .set('Content-Type', 'application/json')
            .send({commandName: 'stolen', commandValue: !curStolenState})
            .end(function(err, res) {
                console.log('changed scooter stolen mode ' + scooterId);
                scooter.stolen = !curStolenState;

                AppDispatcher.handleAction({
                    actionType: AppConstants.SET_SCOOTER,
                    data: scooter
                });
            });
    },

    saveScooter: function(scooter, scooterId, successCalback) {
        console.log('saving scooter ' + scooterId);

        var json_str = JSON.stringify(scooter);

        request.post('/api/scooters/' + scooterId)
            .set('Content-Type', 'application/json')
            .send(json_str)
            .end(function(err, res) {
                console.log('updated scooter ' + scooterId);

                var data = JSON.parse(res.text);

                AppDispatcher.handleAction({
                    actionType: AppConstants.SET_SCOOTER,
                    data: data.result
                });

                if(successCalback) {
                    successCalback();
                }
            })
    }
};

export default ScooterActions;
