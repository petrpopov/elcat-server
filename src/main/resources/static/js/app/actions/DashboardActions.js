/**
 * Created by petrpopov on 24/09/2016.
 */
import AppDispatcher from '../dispatcher/AppDispatcher';
import AppConstants from '../constants/AppConstants';
import request from 'superagent';

var DashboardActions = {

    getDashboardInfo: function() {
        request
            .get('/api/dashboard')
            .end(function(err, res) {
                var data = JSON.parse(res.text);

                AppDispatcher.handleAction({
                    actionType: AppConstants.SET_DASHBOARD,
                    data: data.result
                });
            });
    },

    setItem: function(item){
        AppDispatcher.handleAction({
            actionType: AppConstants.SET_DASHBOARD,
            data: item
        });
    },

    removeItem: function(item){
        AppDispatcher.handleAction({
            actionType: AppConstants.REMOVE_DASHBOARD,
            data: item
        })
    },
};

export default DashboardActions;
