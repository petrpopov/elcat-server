/**
 * Created by petrpopov on 20/09/16.
 */
import AppDispatcher from '../dispatcher/AppDispatcher'
import EventEmitter from 'events';
import AppConstants from '../constants/AppConstants';
import assign from 'object-assign';

var CHANGE_EVENT = 'change';

var _store = {
};

var setItem = function(item){
    _store = item;
};

var removeItem = function(item){
    _store = {};
};

var ScooterStore = assign({}, EventEmitter.prototype, {
    addChangeListener: function(cb){
        this.on(CHANGE_EVENT, cb);
    },
    removeChangeListener: function(cb){
        this.removeListener(CHANGE_EVENT, cb);
    },
    get: function(){
        return _store;
    }
});

AppDispatcher.register(function(payload){
    var action = payload.action;

    switch(action.actionType){
        case AppConstants.SET_SCOOTER:
            setItem(action.data);
            ScooterStore.emit(CHANGE_EVENT);
            break;
        case AppConstants.REMOVE_SCOOTER:
            removeItem(action.data);
            ScooterStore.emit(CHANGE_EVENT);
            break;
        default:
            return true;
    }
});

export default ScooterStore;