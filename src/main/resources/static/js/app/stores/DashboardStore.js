/**
 * Created by petrpopov on 24/09/2016.
 */
import AppDispatcher from '../dispatcher/AppDispatcher'
import EventEmitter from 'events';
import AppConstants from '../constants/AppConstants';
import assign from 'object-assign';

var CHANGE_EVENT = 'change_dashboard';

var _store = {
};

var setItem = function(item){
    _store = item;
};

var removeItem = function(item){
    _store = {};
};

var DashboardStore = assign({}, EventEmitter.prototype, {
    addChangeListener: function(cb){
        this.on(CHANGE_EVENT, cb);
    },
    removeChangeListener: function(cb){
        this.removeListener(CHANGE_EVENT, cb);
    },
    get: function(){
        return _store;
    }
});

AppDispatcher.register(function(payload){
    var action = payload.action;

    switch(action.actionType){
        case AppConstants.SET_DASHBOARD:
            setItem(action.data);
            DashboardStore.emit(CHANGE_EVENT);
            break;
        case AppConstants.REMOVE_DASHBOARD:
            removeItem(action.data);
            DashboardStore.emit(CHANGE_EVENT);
            break;
        default:
            return true;
    }
});

export default DashboardStore;