/**
 * Created by petrpopov on 14/09/16.
 */
'use strict';

import React from 'react';
import { Router, Route, browserHistory, IndexRoute } from 'react-router'
import IndexPage from './js/app/components/pages/IndexPage'
import DashboardPage from './js/app/components/pages/DashboardPage'
import ScootersPage from './js/app/components/pages/ScootersPage'
import ScooterPage from './js/app/components/pages/ScooterPage'
import ScooterEditPage from './js/app/components/ScooterEditPage'

var App = React.createClass({
    render: function() {
        return (
            <h1>HELLO</h1>
        );
    }
});

React.render((
    <Router history={browserHistory} component={IndexPage}>
        <Route path="/" component={IndexPage}>

            <IndexRoute component={DashboardPage}/>

            <Route path="/dashboard" component={DashboardPage}/>
            <Route path="/scooters" component={ScootersPage}/>
            <Route path="/scooters/:scooterId" component={ScooterPage}/>
            <Route path="/scooters/:scooterId/edit" component={ScooterEditPage}/>
        </Route>
    </Router>
), document.getElementById('app'));