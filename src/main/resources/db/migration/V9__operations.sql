ALTER TABLE operations
ADD COLUMN direction character varying(250);

update operations set direction='FROM_SCOOTER';

alter TABLE operations ALTER COLUMN direction SET NOT NULL ;