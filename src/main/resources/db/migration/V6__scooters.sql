ALTER TABLE scooters
DROP COLUMN light_enabled;
ALTER TABLE scooters
DROP COLUMN horn_enabled;
ALTER TABLE scooters
DROP COLUMN brake_enabled;
ALTER TABLE scooters
DROP COLUMN acceleration_enabled;
ALTER TABLE scooters
DROP COLUMN time_limit;
ALTER TABLE scooters RENAME speed_limit  TO last_speed;
ALTER TABLE scooters
ALTER COLUMN last_speed SET DEFAULT '0'::integer;
ALTER TABLE scooters RENAME power_enabled  TO power_enable;
