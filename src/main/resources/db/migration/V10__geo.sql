ALTER TABLE scooters
  ADD COLUMN last_location geometry;

ALTER TABLE operations
  ADD COLUMN location geometry;
