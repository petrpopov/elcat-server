ALTER TABLE trips
ADD COLUMN completed boolean;

update trips set completed=false;

ALTER TABLE trips
ALTER COLUMN completed SET NOT NULL;
ALTER TABLE trips
    ALTER COLUMN completed set DEFAULT FALSE ;