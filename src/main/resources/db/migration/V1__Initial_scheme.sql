--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.0

-- Started on 2016-09-05 12:01:59 MSK

-- SET statement_timeout = 0;
-- SET lock_timeout = 0;
-- SET client_encoding = 'UTF8';
-- SET standard_conforming_strings = on;
-- SET check_function_bodies = false;
-- SET client_min_messages = warning;
-- SET row_security = off;

--
-- TOC entry 7 (class 2615 OID 24676)
-- Name: elcat; Type: SCHEMA; Schema: -; Owner: elcat
--

-- CREATE SCHEMA elcat;


ALTER SCHEMA elcat OWNER TO elcat;

-- create EXTENSION postgis SCHEMA elcat;

SET search_path = elcat, pg_catalog, public;

--
-- TOC entry 190 (class 1259 OID 24815)
-- Name: operations_sequence; Type: SEQUENCE; Schema: elcat; Owner: elcat
--

CREATE SEQUENCE operations_sequence
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


ALTER TABLE operations_sequence OWNER TO elcat;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 191 (class 1259 OID 24817)
-- Name: operations; Type: TABLE; Schema: elcat; Owner: elcat
--

CREATE TABLE operations (
  id bigint DEFAULT nextval('operations_sequence'::regclass) NOT NULL,
  scooter_id bigint NOT NULL,
  received timestamp without time zone NOT NULL,
  scooter_timestamp timestamp without time zone,
  command jsonb NOT NULL,
  created_at timestamp without time zone NOT NULL,
  updated_at timestamp without time zone NOT NULL
);


ALTER TABLE operations OWNER TO elcat;

--
-- TOC entry 185 (class 1259 OID 24702)
-- Name: roles_sequence; Type: SEQUENCE; Schema: elcat; Owner: elcat
--

CREATE SEQUENCE roles_sequence
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


ALTER TABLE roles_sequence OWNER TO elcat;

--
-- TOC entry 186 (class 1259 OID 24704)
-- Name: roles; Type: TABLE; Schema: elcat; Owner: elcat
--

CREATE TABLE roles (
  id bigint DEFAULT nextval('roles_sequence'::regclass) NOT NULL,
  title character varying(250) NOT NULL,
  created_at timestamp without time zone NOT NULL,
  updated_at timestamp without time zone NOT NULL
);


ALTER TABLE roles OWNER TO elcat;

--
-- TOC entry 181 (class 1259 OID 24677)
-- Name: scooters_sequence; Type: SEQUENCE; Schema: elcat; Owner: elcat
--

CREATE SEQUENCE scooters_sequence
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


ALTER TABLE scooters_sequence OWNER TO elcat;

--
-- TOC entry 182 (class 1259 OID 24679)
-- Name: scooters; Type: TABLE; Schema: elcat; Owner: elcat
--

CREATE TABLE scooters (
  id bigint DEFAULT nextval('scooters_sequence'::regclass) NOT NULL,
  cpuid character varying(250) NOT NULL,
  title character varying(250) NOT NULL,
  debug boolean DEFAULT false NOT NULL,
  speed_limit bigint DEFAULT '-1'::integer NOT NULL,
  light_enabled boolean,
  horn_enabled boolean,
  brake_enabled boolean,
  acceleration_enabled boolean,
  time_limit bigint,
  odometer bigint,
  active boolean DEFAULT false NOT NULL,
  stolen boolean DEFAULT false NOT NULL,
  horn_time bigint DEFAULT '-1'::integer NOT NULL,
  wheel_radius bigint DEFAULT 600 NOT NULL,
  connected boolean DEFAULT false NOT NULL,
  min_voltage bigint DEFAULT '-1'::integer NOT NULL,
  max_voltage bigint DEFAULT '-1'::integer NOT NULL,
  power_enaled boolean DEFAULT false NOT NULL,
  created_at timestamp without time zone NOT NULL,
  updated_at timestamp without time zone NOT NULL
);


ALTER TABLE scooters OWNER TO elcat;

--
-- TOC entry 2445 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN scooters.time_limit; Type: COMMENT; Schema: elcat; Owner: elcat
--

COMMENT ON COLUMN scooters.time_limit IS 'in seconds';


--
-- TOC entry 2446 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN scooters.horn_time; Type: COMMENT; Schema: elcat; Owner: elcat
--

COMMENT ON COLUMN scooters.horn_time IS 'milliseconds';


--
-- TOC entry 2447 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN scooters.wheel_radius; Type: COMMENT; Schema: elcat; Owner: elcat
--

COMMENT ON COLUMN scooters.wheel_radius IS 'in millimeters';


--
-- TOC entry 188 (class 1259 OID 24802)
-- Name: trips_sequence; Type: SEQUENCE; Schema: elcat; Owner: elcat
--

CREATE SEQUENCE trips_sequence
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


ALTER TABLE trips_sequence OWNER TO elcat;

--
-- TOC entry 189 (class 1259 OID 24804)
-- Name: trips; Type: TABLE; Schema: elcat; Owner: elcat
--

CREATE TABLE trips (
  id bigint DEFAULT nextval('trips_sequence'::regclass) NOT NULL,
  scooter_id bigint NOT NULL,
  start_date timestamp without time zone NOT NULL,
  end_date timestamp without time zone,
  duration bigint,
  created_at timestamp without time zone NOT NULL,
  updated_at timestamp without time zone NOT NULL
);


ALTER TABLE trips OWNER TO elcat;

--
-- TOC entry 2448 (class 0 OID 0)
-- Dependencies: 189
-- Name: COLUMN trips.duration; Type: COMMENT; Schema: elcat; Owner: elcat
--

COMMENT ON COLUMN trips.duration IS 'in seconds';


--
-- TOC entry 183 (class 1259 OID 24691)
-- Name: users_sequence; Type: SEQUENCE; Schema: elcat; Owner: elcat
--

CREATE SEQUENCE users_sequence
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


ALTER TABLE users_sequence OWNER TO elcat;

--
-- TOC entry 184 (class 1259 OID 24693)
-- Name: users; Type: TABLE; Schema: elcat; Owner: elcat
--

CREATE TABLE users (
  id bigint DEFAULT nextval('users_sequence'::regclass) NOT NULL,
  email character varying(250) NOT NULL,
  active boolean DEFAULT true NOT NULL,
  created_at timestamp without time zone NOT NULL,
  updated_at timestamp without time zone NOT NULL
);


ALTER TABLE users OWNER TO elcat;

--
-- TOC entry 187 (class 1259 OID 24712)
-- Name: users_roles; Type: TABLE; Schema: elcat; Owner: elcat
--

CREATE TABLE users_roles (
  user_id bigint NOT NULL,
  role_id bigint NOT NULL
);


ALTER TABLE users_roles OWNER TO elcat;

--
-- TOC entry 2440 (class 0 OID 24817)
-- Dependencies: 191
-- Data for Name: operations; Type: TABLE DATA; Schema: elcat; Owner: elcat
--

COPY operations (id, scooter_id, received, scooter_timestamp, command, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 2449 (class 0 OID 0)
-- Dependencies: 190
-- Name: operations_sequence; Type: SEQUENCE SET; Schema: elcat; Owner: elcat
--

SELECT pg_catalog.setval('operations_sequence', 1, false);


--
-- TOC entry 2435 (class 0 OID 24704)
-- Dependencies: 186
-- Data for Name: roles; Type: TABLE DATA; Schema: elcat; Owner: elcat
--

COPY roles (id, title, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 2450 (class 0 OID 0)
-- Dependencies: 185
-- Name: roles_sequence; Type: SEQUENCE SET; Schema: elcat; Owner: elcat
--

SELECT pg_catalog.setval('roles_sequence', 1, false);


--
-- TOC entry 2431 (class 0 OID 24679)
-- Dependencies: 182
-- Data for Name: scooters; Type: TABLE DATA; Schema: elcat; Owner: elcat
--

COPY scooters (id, cpuid, title, debug, speed_limit, light_enabled, horn_enabled, brake_enabled, acceleration_enabled, time_limit, odometer, active, stolen, horn_time, wheel_radius, connected, min_voltage, max_voltage, power_enaled, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 2451 (class 0 OID 0)
-- Dependencies: 181
-- Name: scooters_sequence; Type: SEQUENCE SET; Schema: elcat; Owner: elcat
--

SELECT pg_catalog.setval('scooters_sequence', 1, false);


--
-- TOC entry 2438 (class 0 OID 24804)
-- Dependencies: 189
-- Data for Name: trips; Type: TABLE DATA; Schema: elcat; Owner: elcat
--

COPY trips (id, scooter_id, start_date, end_date, duration, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 2452 (class 0 OID 0)
-- Dependencies: 188
-- Name: trips_sequence; Type: SEQUENCE SET; Schema: elcat; Owner: elcat
--

SELECT pg_catalog.setval('trips_sequence', 1, false);


--
-- TOC entry 2433 (class 0 OID 24693)
-- Dependencies: 184
-- Data for Name: users; Type: TABLE DATA; Schema: elcat; Owner: elcat
--

COPY users (id, email, active, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 2436 (class 0 OID 24712)
-- Dependencies: 187
-- Data for Name: users_roles; Type: TABLE DATA; Schema: elcat; Owner: elcat
--

COPY users_roles (user_id, role_id) FROM stdin;
\.


--
-- TOC entry 2453 (class 0 OID 0)
-- Dependencies: 183
-- Name: users_sequence; Type: SEQUENCE SET; Schema: elcat; Owner: elcat
--

SELECT pg_catalog.setval('users_sequence', 1, false);


--
-- TOC entry 2311 (class 2606 OID 24825)
-- Name: operations_pkey; Type: CONSTRAINT; Schema: elcat; Owner: elcat
--

ALTER TABLE ONLY operations
ADD CONSTRAINT operations_pkey PRIMARY KEY (id);


--
-- TOC entry 2303 (class 2606 OID 24709)
-- Name: roles_pkey; Type: CONSTRAINT; Schema: elcat; Owner: elcat
--

ALTER TABLE ONLY roles
ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- TOC entry 2305 (class 2606 OID 24711)
-- Name: roles_title_key; Type: CONSTRAINT; Schema: elcat; Owner: elcat
--

ALTER TABLE ONLY roles
ADD CONSTRAINT roles_title_key UNIQUE (title);


--
-- TOC entry 2295 (class 2606 OID 24690)
-- Name: scooters_cpuid_key; Type: CONSTRAINT; Schema: elcat; Owner: elcat
--

ALTER TABLE ONLY scooters
ADD CONSTRAINT scooters_cpuid_key UNIQUE (cpuid);


--
-- TOC entry 2297 (class 2606 OID 24688)
-- Name: scooters_pkey; Type: CONSTRAINT; Schema: elcat; Owner: elcat
--

ALTER TABLE ONLY scooters
ADD CONSTRAINT scooters_pkey PRIMARY KEY (id);


--
-- TOC entry 2309 (class 2606 OID 24809)
-- Name: trips_pkey; Type: CONSTRAINT; Schema: elcat; Owner: elcat
--

ALTER TABLE ONLY trips
ADD CONSTRAINT trips_pkey PRIMARY KEY (id);


--
-- TOC entry 2299 (class 2606 OID 24701)
-- Name: users_email_key; Type: CONSTRAINT; Schema: elcat; Owner: elcat
--

ALTER TABLE ONLY users
ADD CONSTRAINT users_email_key UNIQUE (email);


--
-- TOC entry 2301 (class 2606 OID 24699)
-- Name: users_pkey; Type: CONSTRAINT; Schema: elcat; Owner: elcat
--

ALTER TABLE ONLY users
ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 2307 (class 2606 OID 24716)
-- Name: users_roles_pkey; Type: CONSTRAINT; Schema: elcat; Owner: elcat
--

ALTER TABLE ONLY users_roles
ADD CONSTRAINT users_roles_pkey PRIMARY KEY (user_id, role_id);


--
-- TOC entry 2315 (class 2606 OID 24826)
-- Name: operations_scooter_id_fkey; Type: FK CONSTRAINT; Schema: elcat; Owner: elcat
--

ALTER TABLE ONLY operations
ADD CONSTRAINT operations_scooter_id_fkey FOREIGN KEY (scooter_id) REFERENCES scooters(id);


--
-- TOC entry 2314 (class 2606 OID 24810)
-- Name: trips_scooter_id_fkey; Type: FK CONSTRAINT; Schema: elcat; Owner: elcat
--

ALTER TABLE ONLY trips
ADD CONSTRAINT trips_scooter_id_fkey FOREIGN KEY (scooter_id) REFERENCES scooters(id);


--
-- TOC entry 2312 (class 2606 OID 24717)
-- Name: users_roles_role_id_fkey; Type: FK CONSTRAINT; Schema: elcat; Owner: elcat
--

ALTER TABLE ONLY users_roles
ADD CONSTRAINT users_roles_role_id_fkey FOREIGN KEY (role_id) REFERENCES roles(id);


--
-- TOC entry 2313 (class 2606 OID 24722)
-- Name: users_roles_user_id_fkey; Type: FK CONSTRAINT; Schema: elcat; Owner: elcat
--

ALTER TABLE ONLY users_roles
ADD CONSTRAINT users_roles_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


-- Completed on 2016-09-05 12:01:59 MSK

--
-- PostgreSQL database dump complete
--

