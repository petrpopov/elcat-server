ALTER TABLE scooters
ADD COLUMN battery_level bigint;

update scooters set battery_level=-1;

ALTER TABLE scooters
    ALTER COLUMN battery_level SET DEFAULT -1;

ALTER TABLE scooters
ALTER COLUMN battery_level SET NOT NULL;

ALTER TABLE scooters
ADD COLUMN head_light bigint;

UPDATE scooters set head_light=-1;

ALTER TABLE scooters
ALTER COLUMN head_light SET DEFAULT -1;

ALTER TABLE scooters
ALTER COLUMN head_light SET NOT NULL;
